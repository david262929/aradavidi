Version 1.4
    Compatibility with WP 4.9.7
    Updated plugins to their latest versions
    Made forms GDPR-compliant
    Added GDPR Framework plugin
    Added alert to Demo data installation
    Compatibility with Gutenberg and other PageBuilders
	Updated documentation


Version 1.3
    Capability with WP 4.9.5
    Transference importer
    Updated plugins to their latest versions

Version 1.2
	Compatibility with PHP7.1
	Updated Revolution Slider to its latest version
	Updated WPBakery PageBuilder to its latest version
	Updated Essential Grid to its latest version
	Updated Booked to its latest version
	Fixed Essential Grid bugs
	Fixed shortcode WPBakery PageBuilder in PHP7.1
	Fixed activated plugins bugs
	Fixed Woocommerce
	Added new CSS styles
	\themes\pabloguadi\shortcodes\trx_basic\section.php

Version 1.1
	All plugins updated.
	Add woocommerce 3.0 support.

Version 1.0
