<?php
/* Gutenberg support functions
------------------------------------------------------------------------------- */

// Theme init
if (!function_exists('pabloguadi_gutenberg_theme_setup')) {
    add_action( 'pabloguadi_action_before_init_theme', 'pabloguadi_gutenberg_theme_setup', 1 );
    function pabloguadi_gutenberg_theme_setup() {
        if (is_admin()) {
            add_filter( 'pabloguadi_filter_required_plugins', 'pabloguadi_gutenberg_required_plugins' );
        }
    }
}

// Check if Instagram Widget installed and activated
if ( !function_exists( 'pabloguadi_exists_gutenberg' ) ) {
    function pabloguadi_exists_gutenberg() {
        return function_exists( 'the_gutenberg_project' ) && function_exists( 'register_block_type' );
    }
}

// Filter to add in the required plugins list
if ( !function_exists( 'pabloguadi_gutenberg_required_plugins' ) ) {
    //add_filter('pabloguadi_filter_required_plugins',    'pabloguadi_gutenberg_required_plugins');
    function pabloguadi_gutenberg_required_plugins($list=array()) {
        if (in_array('gutenberg', (array)pabloguadi_storage_get('required_plugins')))
            $list[] = array(
                'name'         => esc_html__('Gutenberg', 'pabloguadi'),
                'slug'         => 'gutenberg',
                'required'     => false
            );
        return $list;
    }
}