<?php
/* The GDPR Framework support functions
------------------------------------------------------------------------------- */

// Theme init
if (!function_exists('pabloguadi_gdpr_framework_theme_setup')) {
    add_action( 'pabloguadi_action_before_init_theme', 'pabloguadi_gdpr_framework_theme_setup', 1 );
    function pabloguadi_gdpr_framework_theme_setup() {
        if (is_admin()) {
            add_filter( 'pabloguadi_filter_required_plugins', 'pabloguadi_gdpr_framework_required_plugins' );
        }
    }
}

// Check if Instagram Widget installed and activated
if ( !function_exists( 'pabloguadi_exists_gdpr_framework' ) ) {
    function pabloguadi_exists_gdpr_framework() {
        return defined( 'GDPR_FRAMEWORK_VERSION' );
    }
}

// Filter to add in the required plugins list
if ( !function_exists( 'pabloguadi_gdpr_framework_required_plugins' ) ) {
    //add_filter('pabloguadi_filter_required_plugins',    'pabloguadi_gdpr_framework_required_plugins');
    function pabloguadi_gdpr_framework_required_plugins($list=array()) {
        if (in_array('gdpr_framework', (array)pabloguadi_storage_get('required_plugins')))
            $list[] = array(
                'name'         => esc_html__('The GDPR Framework', 'pabloguadi'),
                'slug'         => 'gdpr-framework',
                'required'     => false
            );
        return $list;
    }
}