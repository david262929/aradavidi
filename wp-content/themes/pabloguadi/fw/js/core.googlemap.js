function pabloguadi_googlemap_init(dom_obj, coords) {
	"use strict";
	if (typeof PABLOGUADI_STORAGE['googlemap_init_obj'] == 'undefined') pabloguadi_googlemap_init_styles();
	PABLOGUADI_STORAGE['googlemap_init_obj'].geocoder = '';
	try {
		var id = dom_obj.id;
		PABLOGUADI_STORAGE['googlemap_init_obj'][id] = {
			dom: dom_obj,
			markers: coords.markers,
			geocoder_request: false,
			opt: {
				zoom: coords.zoom,
				center: null,
				scrollwheel: false,
				scaleControl: false,
				disableDefaultUI: false,
				panControl: true,
				zoomControl: true, //zoom
				mapTypeControl: false,
				streetViewControl: false,
				overviewMapControl: false,
				styles: PABLOGUADI_STORAGE['googlemap_styles'][coords.style ? coords.style : 'default'],
				mapTypeId: google.maps.MapTypeId.ROADMAP
			}
		};
		
		pabloguadi_googlemap_create(id);

	} catch (e) {
		
		dcl(PABLOGUADI_STORAGE['strings']['googlemap_not_avail']);

	};
}

function pabloguadi_googlemap_create(id) {
	"use strict";

	// Create map
	PABLOGUADI_STORAGE['googlemap_init_obj'][id].map = new google.maps.Map(PABLOGUADI_STORAGE['googlemap_init_obj'][id].dom, PABLOGUADI_STORAGE['googlemap_init_obj'][id].opt);

	// Add markers
	for (var i in PABLOGUADI_STORAGE['googlemap_init_obj'][id].markers)
		PABLOGUADI_STORAGE['googlemap_init_obj'][id].markers[i].inited = false;
	pabloguadi_googlemap_add_markers(id);
	
	// Add resize listener
	jQuery(window).resize(function() {
		if (PABLOGUADI_STORAGE['googlemap_init_obj'][id].map)
			PABLOGUADI_STORAGE['googlemap_init_obj'][id].map.setCenter(PABLOGUADI_STORAGE['googlemap_init_obj'][id].opt.center);
	});
}

function pabloguadi_googlemap_add_markers(id) {
	"use strict";
	for (var i in PABLOGUADI_STORAGE['googlemap_init_obj'][id].markers) {
		
		if (PABLOGUADI_STORAGE['googlemap_init_obj'][id].markers[i].inited) continue;
		
		if (PABLOGUADI_STORAGE['googlemap_init_obj'][id].markers[i].latlng == '') {
			
			if (PABLOGUADI_STORAGE['googlemap_init_obj'][id].geocoder_request!==false) continue;
			
			if (PABLOGUADI_STORAGE['googlemap_init_obj'].geocoder == '') PABLOGUADI_STORAGE['googlemap_init_obj'].geocoder = new google.maps.Geocoder();
			PABLOGUADI_STORAGE['googlemap_init_obj'][id].geocoder_request = i;
			PABLOGUADI_STORAGE['googlemap_init_obj'].geocoder.geocode({address: PABLOGUADI_STORAGE['googlemap_init_obj'][id].markers[i].address}, function(results, status) {
				"use strict";
				if (status == google.maps.GeocoderStatus.OK) {
					var idx = PABLOGUADI_STORAGE['googlemap_init_obj'][id].geocoder_request;
					if (results[0].geometry.location.lat && results[0].geometry.location.lng) {
						PABLOGUADI_STORAGE['googlemap_init_obj'][id].markers[idx].latlng = '' + results[0].geometry.location.lat() + ',' + results[0].geometry.location.lng();
					} else {
						PABLOGUADI_STORAGE['googlemap_init_obj'][id].markers[idx].latlng = results[0].geometry.location.toString().replace(/\(\)/g, '');
					}
					PABLOGUADI_STORAGE['googlemap_init_obj'][id].geocoder_request = false;
					setTimeout(function() { 
						pabloguadi_googlemap_add_markers(id); 
						}, 200);
				} else
					dcl(PABLOGUADI_STORAGE['strings']['geocode_error'] + ' ' + status);
			});
		
		} else {
			
			// Prepare marker object
			var latlngStr = PABLOGUADI_STORAGE['googlemap_init_obj'][id].markers[i].latlng.split(',');
			var markerInit = {
				map: PABLOGUADI_STORAGE['googlemap_init_obj'][id].map,
				position: new google.maps.LatLng(latlngStr[0], latlngStr[1]),
				clickable: PABLOGUADI_STORAGE['googlemap_init_obj'][id].markers[i].description!=''
			};
			if (PABLOGUADI_STORAGE['googlemap_init_obj'][id].markers[i].point) markerInit.icon = PABLOGUADI_STORAGE['googlemap_init_obj'][id].markers[i].point;
			if (PABLOGUADI_STORAGE['googlemap_init_obj'][id].markers[i].title) markerInit.title = PABLOGUADI_STORAGE['googlemap_init_obj'][id].markers[i].title;
			PABLOGUADI_STORAGE['googlemap_init_obj'][id].markers[i].marker = new google.maps.Marker(markerInit);
			
			// Set Map center
			if (PABLOGUADI_STORAGE['googlemap_init_obj'][id].opt.center == null) {
				PABLOGUADI_STORAGE['googlemap_init_obj'][id].opt.center = markerInit.position;
				PABLOGUADI_STORAGE['googlemap_init_obj'][id].map.setCenter(PABLOGUADI_STORAGE['googlemap_init_obj'][id].opt.center);				
			}
			
			// Add description window
			if (PABLOGUADI_STORAGE['googlemap_init_obj'][id].markers[i].description!='') {
				PABLOGUADI_STORAGE['googlemap_init_obj'][id].markers[i].infowindow = new google.maps.InfoWindow({
					content: PABLOGUADI_STORAGE['googlemap_init_obj'][id].markers[i].description
				});
				google.maps.event.addListener(PABLOGUADI_STORAGE['googlemap_init_obj'][id].markers[i].marker, "click", function(e) {
					var latlng = e.latLng.toString().replace("(", '').replace(")", "").replace(" ", "");
					for (var i in PABLOGUADI_STORAGE['googlemap_init_obj'][id].markers) {
						if (latlng == PABLOGUADI_STORAGE['googlemap_init_obj'][id].markers[i].latlng) {
							PABLOGUADI_STORAGE['googlemap_init_obj'][id].markers[i].infowindow.open(
								PABLOGUADI_STORAGE['googlemap_init_obj'][id].map,
								PABLOGUADI_STORAGE['googlemap_init_obj'][id].markers[i].marker
							);
							break;
						}
					}
				});
			}
			
			PABLOGUADI_STORAGE['googlemap_init_obj'][id].markers[i].inited = true;
		}
	}
}

function pabloguadi_googlemap_refresh() {
	"use strict";
	for (id in PABLOGUADI_STORAGE['googlemap_init_obj']) {
		pabloguadi_googlemap_create(id);
	}
}

function pabloguadi_googlemap_init_styles() {
	// Init Google map
	PABLOGUADI_STORAGE['googlemap_init_obj'] = {};
	PABLOGUADI_STORAGE['googlemap_styles'] = {
		'default': []
	};
	if (window.pabloguadi_theme_googlemap_styles!==undefined)
		PABLOGUADI_STORAGE['googlemap_styles'] = pabloguadi_theme_googlemap_styles(PABLOGUADI_STORAGE['googlemap_styles']);
}