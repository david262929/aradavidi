<?php
/**
 * PabloGuadi Framework: messages subsystem
 *
 * @package	pabloguadi
 * @since	pabloguadi 1.0
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }

// Theme init
if (!function_exists('pabloguadi_messages_theme_setup')) {
	add_action( 'pabloguadi_action_before_init_theme', 'pabloguadi_messages_theme_setup' );
	function pabloguadi_messages_theme_setup() {
		// Core messages strings
		add_filter('pabloguadi_filter_localize_script', 'pabloguadi_messages_localize_script');
	}
}


/* Session messages
------------------------------------------------------------------------------------- */

if (!function_exists('pabloguadi_get_error_msg')) {
	function pabloguadi_get_error_msg() {
		return pabloguadi_storage_get('error_msg');
	}
}

if (!function_exists('pabloguadi_set_error_msg')) {
	function pabloguadi_set_error_msg($msg) {
		$msg2 = pabloguadi_get_error_msg();
		pabloguadi_storage_set('error_msg', trim($msg2) . ($msg2=='' ? '' : '<br />') . trim($msg));
	}
}

if (!function_exists('pabloguadi_get_success_msg')) {
	function pabloguadi_get_success_msg() {
		return pabloguadi_storage_get('success_msg');
	}
}

if (!function_exists('pabloguadi_set_success_msg')) {
	function pabloguadi_set_success_msg($msg) {
		$msg2 = pabloguadi_get_success_msg();
		pabloguadi_storage_set('success_msg', trim($msg2) . ($msg2=='' ? '' : '<br />') . trim($msg));
	}
}

if (!function_exists('pabloguadi_get_notice_msg')) {
	function pabloguadi_get_notice_msg() {
		return pabloguadi_storage_get('notice_msg');
	}
}

if (!function_exists('pabloguadi_set_notice_msg')) {
	function pabloguadi_set_notice_msg($msg) {
		$msg2 = pabloguadi_get_notice_msg();
		pabloguadi_storage_set('notice_msg', trim($msg2) . ($msg2=='' ? '' : '<br />') . trim($msg));
	}
}


/* System messages (save when page reload)
------------------------------------------------------------------------------------- */
if (!function_exists('pabloguadi_set_system_message')) {
	function pabloguadi_set_system_message($msg, $status='info', $hdr='') {
		update_option(pabloguadi_storage_get('options_prefix') . '_message', array('message' => $msg, 'status' => $status, 'header' => $hdr));
	}
}

if (!function_exists('pabloguadi_get_system_message')) {
	function pabloguadi_get_system_message($del=false) {
		$msg = get_option(pabloguadi_storage_get('options_prefix') . '_message', false);
		if (!$msg)
			$msg = array('message' => '', 'status' => '', 'header' => '');
		else if ($del)
			pabloguadi_del_system_message();
		return $msg;
	}
}

if (!function_exists('pabloguadi_del_system_message')) {
	function pabloguadi_del_system_message() {
		delete_option(pabloguadi_storage_get('options_prefix') . '_message');
	}
}


/* Messages strings
------------------------------------------------------------------------------------- */

if (!function_exists('pabloguadi_messages_localize_script')) {
	//add_filter('pabloguadi_filter_localize_script', 'pabloguadi_messages_localize_script');
	function pabloguadi_messages_localize_script($vars) {
		$vars['strings'] = array(
			'ajax_error'		=> esc_html__('Invalid server answer', 'pabloguadi'),
			'bookmark_add'		=> esc_html__('Add the bookmark', 'pabloguadi'),
            'bookmark_added'	=> esc_html__('Current page has been successfully added to the bookmarks. You can see it in the right panel on the tab \'Bookmarks\'', 'pabloguadi'),
            'bookmark_del'		=> esc_html__('Delete this bookmark', 'pabloguadi'),
            'bookmark_title'	=> esc_html__('Enter bookmark title', 'pabloguadi'),
            'bookmark_exists'	=> esc_html__('Current page already exists in the bookmarks list', 'pabloguadi'),
			'search_error'		=> esc_html__('Error occurs in AJAX search! Please, type your query and press search icon for the traditional search way.', 'pabloguadi'),
			'email_confirm'		=> esc_html__('On the e-mail address "%s" we sent a confirmation email. Please, open it and click on the link.', 'pabloguadi'),
			'reviews_vote'		=> esc_html__('Thanks for your vote! New average rating is:', 'pabloguadi'),
			'reviews_error'		=> esc_html__('Error saving your vote! Please, try again later.', 'pabloguadi'),
			'error_like'		=> esc_html__('Error saving your like! Please, try again later.', 'pabloguadi'),
			'error_global'		=> esc_html__('Global error text', 'pabloguadi'),
			'name_empty'		=> esc_html__('The name can\'t be empty', 'pabloguadi'),
			'name_long'			=> esc_html__('Too long name', 'pabloguadi'),
			'email_empty'		=> esc_html__('Too short (or empty) email address', 'pabloguadi'),
			'email_long'		=> esc_html__('Too long email address', 'pabloguadi'),
			'email_not_valid'	=> esc_html__('Invalid email address', 'pabloguadi'),
			'subject_empty'		=> esc_html__('The subject can\'t be empty', 'pabloguadi'),
			'subject_long'		=> esc_html__('Too long subject', 'pabloguadi'),
			'text_empty'		=> esc_html__('The message text can\'t be empty', 'pabloguadi'),
			'text_long'			=> esc_html__('Too long message text', 'pabloguadi'),
			'send_complete'		=> esc_html__("Send message complete!", 'pabloguadi'),
			'send_error'		=> esc_html__('Transmit failed!', 'pabloguadi'),
			'geocode_error'			=> esc_html__('Geocode was not successful for the following reason:', 'pabloguadi'),
			'googlemap_not_avail'	=> esc_html__('Google map API not available!', 'pabloguadi'),
			'editor_save_success'	=> esc_html__("Post content saved!", 'pabloguadi'),
			'editor_save_error'		=> esc_html__("Error saving post data!", 'pabloguadi'),
			'editor_delete_post'	=> esc_html__("You really want to delete the current post?", 'pabloguadi'),
			'editor_delete_post_header'	=> esc_html__("Delete post", 'pabloguadi'),
			'editor_delete_success'	=> esc_html__("Post deleted!", 'pabloguadi'),
			'editor_delete_error'	=> esc_html__("Error deleting post!", 'pabloguadi'),
			'editor_caption_cancel'	=> esc_html__('Cancel', 'pabloguadi'),
			'editor_caption_close'	=> esc_html__('Close', 'pabloguadi')
			);
		return $vars;
	}
}
?>