<?php
/**
 * PabloGuadi Framework: return lists
 *
 * @package pabloguadi
 * @since pabloguadi 1.0
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }



// Return styles list
if ( !function_exists( 'pabloguadi_get_list_styles' ) ) {
	function pabloguadi_get_list_styles($from=1, $to=2, $prepend_inherit=false) {
		$list = array();
		for ($i=$from; $i<=$to; $i++)
			$list[$i] = sprintf(esc_html__('Style %d', 'pabloguadi'), $i);
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}


// Return list of the shortcodes margins
if ( !function_exists( 'pabloguadi_get_list_margins' ) ) {
	function pabloguadi_get_list_margins($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_margins'))=='') {
			$list = array(
				'null'		=> esc_html__('0 (No margin)',	'pabloguadi'),
				'tiny'		=> esc_html__('Tiny',		'pabloguadi'),
				'small'		=> esc_html__('Small',		'pabloguadi'),
				'medium'	=> esc_html__('Medium',		'pabloguadi'),
				'large'		=> esc_html__('Large',		'pabloguadi'),
				'huge'		=> esc_html__('Huge',		'pabloguadi'),
				'tiny-'		=> esc_html__('Tiny (negative)',	'pabloguadi'),
				'small-'	=> esc_html__('Small (negative)',	'pabloguadi'),
				'medium-'	=> esc_html__('Medium (negative)',	'pabloguadi'),
				'large-'	=> esc_html__('Large (negative)',	'pabloguadi'),
				'huge-'		=> esc_html__('Huge (negative)',	'pabloguadi')
				);
			$list = apply_filters('pabloguadi_filter_list_margins', $list);
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_margins', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}


// Return list of the line styles
if ( !function_exists( 'pabloguadi_get_list_line_styles' ) ) {
	function pabloguadi_get_list_line_styles($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_line_styles'))=='') {
			$list = array(
				'solid'	=> esc_html__('Solid', 'pabloguadi'),
				'dashed'=> esc_html__('Dashed', 'pabloguadi'),
				'dotted'=> esc_html__('Dotted', 'pabloguadi'),
				'double'=> esc_html__('Double', 'pabloguadi'),
				'image'	=> esc_html__('Image', 'pabloguadi')
				);
			$list = apply_filters('pabloguadi_filter_list_line_styles', $list);
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_line_styles', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}


// Return list of the animations
if ( !function_exists( 'pabloguadi_get_list_animations' ) ) {
	function pabloguadi_get_list_animations($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_animations'))=='') {
			$list = array(
				'none'			=> esc_html__('- None -',	'pabloguadi'),
				'bounce'		=> esc_html__('Bounce',		'pabloguadi'),
				'elastic'		=> esc_html__('Elastic',	'pabloguadi'),
				'flash'			=> esc_html__('Flash',		'pabloguadi'),
				'flip'			=> esc_html__('Flip',		'pabloguadi'),
				'pulse'			=> esc_html__('Pulse',		'pabloguadi'),
				'rubberBand'	=> esc_html__('Rubber Band','pabloguadi'),
				'shake'			=> esc_html__('Shake',		'pabloguadi'),
				'swing'			=> esc_html__('Swing',		'pabloguadi'),
				'tada'			=> esc_html__('Tada',		'pabloguadi'),
				'wobble'		=> esc_html__('Wobble',		'pabloguadi')
				);
			$list = apply_filters('pabloguadi_filter_list_animations', $list);
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_animations', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}


// Return list of the enter animations
if ( !function_exists( 'pabloguadi_get_list_animations_in' ) ) {
	function pabloguadi_get_list_animations_in($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_animations_in'))=='') {
			$list = array(
				'none'				=> esc_html__('- None -',			'pabloguadi'),
				'bounceIn'			=> esc_html__('Bounce In',			'pabloguadi'),
				'bounceInUp'		=> esc_html__('Bounce In Up',		'pabloguadi'),
				'bounceInDown'		=> esc_html__('Bounce In Down',		'pabloguadi'),
				'bounceInLeft'		=> esc_html__('Bounce In Left',		'pabloguadi'),
				'bounceInRight'		=> esc_html__('Bounce In Right',	'pabloguadi'),
				'elastic'			=> esc_html__('Elastic In',			'pabloguadi'),
				'fadeIn'			=> esc_html__('Fade In',			'pabloguadi'),
				'fadeInUp'			=> esc_html__('Fade In Up',			'pabloguadi'),
				'fadeInUpSmall'		=> esc_html__('Fade In Up Small',	'pabloguadi'),
				'fadeInUpBig'		=> esc_html__('Fade In Up Big',		'pabloguadi'),
				'fadeInDown'		=> esc_html__('Fade In Down',		'pabloguadi'),
				'fadeInDownBig'		=> esc_html__('Fade In Down Big',	'pabloguadi'),
				'fadeInLeft'		=> esc_html__('Fade In Left',		'pabloguadi'),
				'fadeInLeftBig'		=> esc_html__('Fade In Left Big',	'pabloguadi'),
				'fadeInRight'		=> esc_html__('Fade In Right',		'pabloguadi'),
				'fadeInRightBig'	=> esc_html__('Fade In Right Big',	'pabloguadi'),
				'flipInX'			=> esc_html__('Flip In X',			'pabloguadi'),
				'flipInY'			=> esc_html__('Flip In Y',			'pabloguadi'),
				'lightSpeedIn'		=> esc_html__('Light Speed In',		'pabloguadi'),
				'rotateIn'			=> esc_html__('Rotate In',			'pabloguadi'),
				'rotateInUpLeft'	=> esc_html__('Rotate In Down Left','pabloguadi'),
				'rotateInUpRight'	=> esc_html__('Rotate In Up Right',	'pabloguadi'),
				'rotateInDownLeft'	=> esc_html__('Rotate In Up Left',	'pabloguadi'),
				'rotateInDownRight'	=> esc_html__('Rotate In Down Right','pabloguadi'),
				'rollIn'			=> esc_html__('Roll In',			'pabloguadi'),
				'slideInUp'			=> esc_html__('Slide In Up',		'pabloguadi'),
				'slideInDown'		=> esc_html__('Slide In Down',		'pabloguadi'),
				'slideInLeft'		=> esc_html__('Slide In Left',		'pabloguadi'),
				'slideInRight'		=> esc_html__('Slide In Right',		'pabloguadi'),
				'wipeInLeftTop'		=> esc_html__('Wipe In Left Top',	'pabloguadi'),
				'zoomIn'			=> esc_html__('Zoom In',			'pabloguadi'),
				'zoomInUp'			=> esc_html__('Zoom In Up',			'pabloguadi'),
				'zoomInDown'		=> esc_html__('Zoom In Down',		'pabloguadi'),
				'zoomInLeft'		=> esc_html__('Zoom In Left',		'pabloguadi'),
				'zoomInRight'		=> esc_html__('Zoom In Right',		'pabloguadi')
				);
			$list = apply_filters('pabloguadi_filter_list_animations_in', $list);
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_animations_in', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}


// Return list of the out animations
if ( !function_exists( 'pabloguadi_get_list_animations_out' ) ) {
	function pabloguadi_get_list_animations_out($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_animations_out'))=='') {
			$list = array(
				'none'				=> esc_html__('- None -',			'pabloguadi'),
				'bounceOut'			=> esc_html__('Bounce Out',			'pabloguadi'),
				'bounceOutUp'		=> esc_html__('Bounce Out Up',		'pabloguadi'),
				'bounceOutDown'		=> esc_html__('Bounce Out Down',	'pabloguadi'),
				'bounceOutLeft'		=> esc_html__('Bounce Out Left',	'pabloguadi'),
				'bounceOutRight'	=> esc_html__('Bounce Out Right',	'pabloguadi'),
				'fadeOut'			=> esc_html__('Fade Out',			'pabloguadi'),
				'fadeOutUp'			=> esc_html__('Fade Out Up',		'pabloguadi'),
				'fadeOutUpBig'		=> esc_html__('Fade Out Up Big',	'pabloguadi'),
				'fadeOutDown'		=> esc_html__('Fade Out Down',		'pabloguadi'),
				'fadeOutDownSmall'	=> esc_html__('Fade Out Down Small','pabloguadi'),
				'fadeOutDownBig'	=> esc_html__('Fade Out Down Big',	'pabloguadi'),
				'fadeOutLeft'		=> esc_html__('Fade Out Left',		'pabloguadi'),
				'fadeOutLeftBig'	=> esc_html__('Fade Out Left Big',	'pabloguadi'),
				'fadeOutRight'		=> esc_html__('Fade Out Right',		'pabloguadi'),
				'fadeOutRightBig'	=> esc_html__('Fade Out Right Big',	'pabloguadi'),
				'flipOutX'			=> esc_html__('Flip Out X',			'pabloguadi'),
				'flipOutY'			=> esc_html__('Flip Out Y',			'pabloguadi'),
				'hinge'				=> esc_html__('Hinge Out',			'pabloguadi'),
				'lightSpeedOut'		=> esc_html__('Light Speed Out',	'pabloguadi'),
				'rotateOut'			=> esc_html__('Rotate Out',			'pabloguadi'),
				'rotateOutUpLeft'	=> esc_html__('Rotate Out Down Left','pabloguadi'),
				'rotateOutUpRight'	=> esc_html__('Rotate Out Up Right','pabloguadi'),
				'rotateOutDownLeft'	=> esc_html__('Rotate Out Up Left',	'pabloguadi'),
				'rotateOutDownRight'=> esc_html__('Rotate Out Down Right','pabloguadi'),
				'rollOut'			=> esc_html__('Roll Out',			'pabloguadi'),
				'slideOutUp'		=> esc_html__('Slide Out Up',		'pabloguadi'),
				'slideOutDown'		=> esc_html__('Slide Out Down',		'pabloguadi'),
				'slideOutLeft'		=> esc_html__('Slide Out Left',		'pabloguadi'),
				'slideOutRight'		=> esc_html__('Slide Out Right',	'pabloguadi'),
				'zoomOut'			=> esc_html__('Zoom Out',			'pabloguadi'),
				'zoomOutUp'			=> esc_html__('Zoom Out Up',		'pabloguadi'),
				'zoomOutDown'		=> esc_html__('Zoom Out Down',		'pabloguadi'),
				'zoomOutLeft'		=> esc_html__('Zoom Out Left',		'pabloguadi'),
				'zoomOutRight'		=> esc_html__('Zoom Out Right',		'pabloguadi')
				);
			$list = apply_filters('pabloguadi_filter_list_animations_out', $list);
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_animations_out', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return classes list for the specified animation
if (!function_exists('pabloguadi_get_animation_classes')) {
	function pabloguadi_get_animation_classes($animation, $speed='normal', $loop='none') {
		// speed:	fast=0.5s | normal=1s | slow=2s
		// loop:	none | infinite
		return pabloguadi_param_is_off($animation) ? '' : 'animated '.esc_attr($animation).' '.esc_attr($speed).(!pabloguadi_param_is_off($loop) ? ' '.esc_attr($loop) : '');
	}
}


// Return list of the main menu hover effects
if ( !function_exists( 'pabloguadi_get_list_menu_hovers' ) ) {
	function pabloguadi_get_list_menu_hovers($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_menu_hovers'))=='') {
			$list = array(
				'fade'			=> esc_html__('Fade',		'pabloguadi'),
				'slide_line'	=> esc_html__('Slide Line',	'pabloguadi'),
				'slide_box'		=> esc_html__('Slide Box',	'pabloguadi'),
				'zoom_line'		=> esc_html__('Zoom Line',	'pabloguadi'),
				'path_line'		=> esc_html__('Path Line',	'pabloguadi'),
				'roll_down'		=> esc_html__('Roll Down',	'pabloguadi'),
				'color_line'	=> esc_html__('Color Line',	'pabloguadi'),
				);
			$list = apply_filters('pabloguadi_filter_list_menu_hovers', $list);
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_menu_hovers', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}


// Return list of the button's hover effects
if ( !function_exists( 'pabloguadi_get_list_button_hovers' ) ) {
	function pabloguadi_get_list_button_hovers($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_button_hovers'))=='') {
			$list = array(
				'default'		=> esc_html__('Default',			'pabloguadi'),
				'fade'			=> esc_html__('Fade',				'pabloguadi'),
				'slide_left'	=> esc_html__('Slide from Left',	'pabloguadi'),
				'slide_top'		=> esc_html__('Slide from Top',		'pabloguadi'),
				'arrow'			=> esc_html__('Arrow',				'pabloguadi'),
				);
			$list = apply_filters('pabloguadi_filter_list_button_hovers', $list);
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_button_hovers', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}


// Return list of the input field's hover effects
if ( !function_exists( 'pabloguadi_get_list_input_hovers' ) ) {
	function pabloguadi_get_list_input_hovers($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_input_hovers'))=='') {
			$list = array(
				'default'	=> esc_html__('Default',	'pabloguadi'),
				'accent'	=> esc_html__('Accented',	'pabloguadi'),
				'path'		=> esc_html__('Path',		'pabloguadi'),
				'jump'		=> esc_html__('Jump',		'pabloguadi'),
				'underline'	=> esc_html__('Underline',	'pabloguadi'),
				'iconed'	=> esc_html__('Iconed',		'pabloguadi'),
				);
			$list = apply_filters('pabloguadi_filter_list_input_hovers', $list);
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_input_hovers', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}


// Return list of the search field's styles
if ( !function_exists( 'pabloguadi_get_list_search_styles' ) ) {
	function pabloguadi_get_list_search_styles($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_search_styles'))=='') {
			$list = array(
				'default'	=> esc_html__('Default',	'pabloguadi'),
				'fullscreen'=> esc_html__('Fullscreen',	'pabloguadi'),
				'slide'		=> esc_html__('Slide',		'pabloguadi'),
				'expand'	=> esc_html__('Expand',		'pabloguadi'),
				);
			$list = apply_filters('pabloguadi_filter_list_search_styles', $list);
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_search_styles', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}


// Return list of categories
if ( !function_exists( 'pabloguadi_get_list_categories' ) ) {
	function pabloguadi_get_list_categories($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_categories'))=='') {
			$list = array();
			$args = array(
				'type'                     => 'post',
				'child_of'                 => 0,
				'parent'                   => '',
				'orderby'                  => 'name',
				'order'                    => 'ASC',
				'hide_empty'               => 0,
				'hierarchical'             => 1,
				'exclude'                  => '',
				'include'                  => '',
				'number'                   => '',
				'taxonomy'                 => 'category',
				'pad_counts'               => false );
			$taxonomies = get_categories( $args );
			if (is_array($taxonomies) && count($taxonomies) > 0) {
				foreach ($taxonomies as $cat) {
					$list[$cat->term_id] = $cat->name;
				}
			}
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_categories', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}


// Return list of taxonomies
if ( !function_exists( 'pabloguadi_get_list_terms' ) ) {
	function pabloguadi_get_list_terms($prepend_inherit=false, $taxonomy='category') {
		if (($list = pabloguadi_storage_get('list_taxonomies_'.($taxonomy)))=='') {
			$list = array();
			if ( is_array($taxonomy) || taxonomy_exists($taxonomy) ) {
				$terms = get_terms( $taxonomy, array(
					'child_of'                 => 0,
					'parent'                   => '',
					'orderby'                  => 'name',
					'order'                    => 'ASC',
					'hide_empty'               => 0,
					'hierarchical'             => 1,
					'exclude'                  => '',
					'include'                  => '',
					'number'                   => '',
					'taxonomy'                 => $taxonomy,
					'pad_counts'               => false
					)
				);
			} else {
				$terms = pabloguadi_get_terms_by_taxonomy_from_db($taxonomy);
			}
			if (!is_wp_error( $terms ) && is_array($terms) && count($terms) > 0) {
				foreach ($terms as $cat) {
					$list[$cat->term_id] = $cat->name;	// . ($taxonomy!='category' ? ' /'.($cat->taxonomy).'/' : '');
				}
			}
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_taxonomies_'.($taxonomy), $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return list of post's types
if ( !function_exists( 'pabloguadi_get_list_posts_types' ) ) {
	function pabloguadi_get_list_posts_types($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_posts_types'))=='') {
			// Return only theme inheritance supported post types
			$list = apply_filters('pabloguadi_filter_list_post_types', array());
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_posts_types', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}


// Return list post items from any post type and taxonomy
if ( !function_exists( 'pabloguadi_get_list_posts' ) ) {
	function pabloguadi_get_list_posts($prepend_inherit=false, $opt=array()) {
		$opt = array_merge(array(
			'post_type'			=> 'post',
			'post_status'		=> 'publish',
			'taxonomy'			=> 'category',
			'taxonomy_value'	=> '',
			'posts_per_page'	=> -1,
			'orderby'			=> 'post_date',
			'order'				=> 'desc',
			'return'			=> 'id'
			), is_array($opt) ? $opt : array('post_type'=>$opt));

		$hash = 'list_posts_'.($opt['post_type']).'_'.($opt['taxonomy']).'_'.($opt['taxonomy_value']).'_'.($opt['orderby']).'_'.($opt['order']).'_'.($opt['return']).'_'.($opt['posts_per_page']);
		if (($list = pabloguadi_storage_get($hash))=='') {
			$list = array();
			$list['none'] = esc_html__("- Not selected -", 'pabloguadi');
			$args = array(
				'post_type' => $opt['post_type'],
				'post_status' => $opt['post_status'],
				'posts_per_page' => $opt['posts_per_page'],
				'ignore_sticky_posts' => true,
				'orderby'	=> $opt['orderby'],
				'order'		=> $opt['order']
			);
			if (!empty($opt['taxonomy_value'])) {
				$args['tax_query'] = array(
					array(
						'taxonomy' => $opt['taxonomy'],
						'field' => (int) $opt['taxonomy_value'] > 0 ? 'id' : 'slug',
						'terms' => $opt['taxonomy_value']
					)
				);
			}
			$posts = get_posts( $args );
			if (is_array($posts) && count($posts) > 0) {
				foreach ($posts as $post) {
					$list[$opt['return']=='id' ? $post->ID : $post->post_title] = $post->post_title;
				}
			}
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set($hash, $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}


// Return list pages
if ( !function_exists( 'pabloguadi_get_list_pages' ) ) {
	function pabloguadi_get_list_pages($prepend_inherit=false, $opt=array()) {
		$opt = array_merge(array(
			'post_type'			=> 'page',
			'post_status'		=> 'publish',
			'posts_per_page'	=> -1,
			'orderby'			=> 'title',
			'order'				=> 'asc',
			'return'			=> 'id'
			), is_array($opt) ? $opt : array('post_type'=>$opt));
		return pabloguadi_get_list_posts($prepend_inherit, $opt);
	}
}


// Return list of registered users
if ( !function_exists( 'pabloguadi_get_list_users' ) ) {
	function pabloguadi_get_list_users($prepend_inherit=false, $roles=array('administrator', 'editor', 'author', 'contributor', 'shop_manager')) {
		if (($list = pabloguadi_storage_get('list_users'))=='') {
			$list = array();
			$list['none'] = esc_html__("- Not selected -", 'pabloguadi');
			$args = array(
				'orderby'	=> 'display_name',
				'order'		=> 'ASC' );
			$users = get_users( $args );
			if (is_array($users) && count($users) > 0) {
				foreach ($users as $user) {
					$accept = true;
					if (is_array($user->roles)) {
						if (is_array($user->roles) && count($user->roles) > 0) {
							$accept = false;
							foreach ($user->roles as $role) {
								if (in_array($role, $roles)) {
									$accept = true;
									break;
								}
							}
						}
					}
					if ($accept) $list[$user->user_login] = $user->display_name;
				}
			}
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_users', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}


// Return slider engines list, prepended inherit (if need)
if ( !function_exists( 'pabloguadi_get_list_sliders' ) ) {
	function pabloguadi_get_list_sliders($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_sliders'))=='') {
			$list = array(
				'swiper' => esc_html__("Posts slider (Swiper)", 'pabloguadi')
			);
			$list = apply_filters('pabloguadi_filter_list_sliders', $list);
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_sliders', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}


// Return slider controls list, prepended inherit (if need)
if ( !function_exists( 'pabloguadi_get_list_slider_controls' ) ) {
	function pabloguadi_get_list_slider_controls($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_slider_controls'))=='') {
			$list = array(
				'no'		=> esc_html__('None', 'pabloguadi'),
				'side'		=> esc_html__('Side', 'pabloguadi'),
				'bottom'	=> esc_html__('Bottom', 'pabloguadi'),
				'pagination'=> esc_html__('Pagination', 'pabloguadi')
				);
			$list = apply_filters('pabloguadi_filter_list_slider_controls', $list);
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_slider_controls', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}


// Return slider controls classes
if ( !function_exists( 'pabloguadi_get_slider_controls_classes' ) ) {
	function pabloguadi_get_slider_controls_classes($controls) {
		if (pabloguadi_param_is_off($controls))	$classes = 'sc_slider_nopagination sc_slider_nocontrols';
		else if ($controls=='bottom')			$classes = 'sc_slider_nopagination sc_slider_controls sc_slider_controls_bottom';
		else if ($controls=='pagination')		$classes = 'sc_slider_pagination sc_slider_pagination_bottom sc_slider_nocontrols';
		else									$classes = 'sc_slider_nopagination sc_slider_controls sc_slider_controls_side';
		return $classes;
	}
}

// Return list with popup engines
if ( !function_exists( 'pabloguadi_get_list_popup_engines' ) ) {
	function pabloguadi_get_list_popup_engines($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_popup_engines'))=='') {
			$list = array(
				"pretty"	=> esc_html__("Pretty photo", 'pabloguadi'),
				"magnific"	=> esc_html__("Magnific popup", 'pabloguadi')
				);
			$list = apply_filters('pabloguadi_filter_list_popup_engines', $list);
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_popup_engines', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return menus list, prepended inherit
if ( !function_exists( 'pabloguadi_get_list_menus' ) ) {
	function pabloguadi_get_list_menus($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_menus'))=='') {
			$list = array();
			$list['default'] = esc_html__("Default", 'pabloguadi');
			$menus = wp_get_nav_menus();
			if (is_array($menus) && count($menus) > 0) {
				foreach ($menus as $menu) {
					$list[$menu->slug] = $menu->name;
				}
			}
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_menus', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return custom sidebars list, prepended inherit and main sidebars item (if need)
if ( !function_exists( 'pabloguadi_get_list_sidebars' ) ) {
	function pabloguadi_get_list_sidebars($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_sidebars'))=='') {
			if (($list = pabloguadi_storage_get('registered_sidebars'))=='') $list = array();
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_sidebars', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return sidebars positions
if ( !function_exists( 'pabloguadi_get_list_sidebars_positions' ) ) {
	function pabloguadi_get_list_sidebars_positions($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_sidebars_positions'))=='') {
			$list = array(
				'none'  => esc_html__('Hide',  'pabloguadi'),
				'left'  => esc_html__('Left',  'pabloguadi'),
				'right' => esc_html__('Right', 'pabloguadi')
				);
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_sidebars_positions', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return sidebars class
if ( !function_exists( 'pabloguadi_get_sidebar_class' ) ) {
	function pabloguadi_get_sidebar_class() {
		$sb_main = pabloguadi_get_custom_option('show_sidebar_main');
		$sb_outer = pabloguadi_get_custom_option('show_sidebar_outer');
		return (pabloguadi_param_is_off($sb_main) ? 'sidebar_hide' : 'sidebar_show sidebar_'.($sb_main))
				. ' ' . (pabloguadi_param_is_off($sb_outer) ? 'sidebar_outer_hide' : 'sidebar_outer_show sidebar_outer_'.($sb_outer));
	}
}

// Return body styles list, prepended inherit
if ( !function_exists( 'pabloguadi_get_list_body_styles' ) ) {
	function pabloguadi_get_list_body_styles($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_body_styles'))=='') {
			$list = array(
				'boxed'	=> esc_html__('Boxed',		'pabloguadi'),
				'wide'	=> esc_html__('Wide',		'pabloguadi')
				);
			if (pabloguadi_get_theme_setting('allow_fullscreen')) {
				$list['fullwide']	= esc_html__('Fullwide',	'pabloguadi');
				$list['fullscreen']	= esc_html__('Fullscreen',	'pabloguadi');
			}
			$list = apply_filters('pabloguadi_filter_list_body_styles', $list);
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_body_styles', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return templates list, prepended inherit
if ( !function_exists( 'pabloguadi_get_list_templates' ) ) {
	function pabloguadi_get_list_templates($mode='') {
		if (($list = pabloguadi_storage_get('list_templates_'.($mode)))=='') {
			$list = array();
			$tpl = pabloguadi_storage_get('registered_templates');
			if (is_array($tpl) && count($tpl) > 0) {
				foreach ($tpl as $k=>$v) {
					if ($mode=='' || in_array($mode, explode(',', $v['mode'])))
						$list[$k] = !empty($v['icon']) 
									? $v['icon'] 
									: (!empty($v['title']) 
										? $v['title'] 
										: pabloguadi_strtoproper($v['layout'])
										);
				}
			}
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_templates_'.($mode), $list);
		}
		return $list;
	}
}

// Return blog styles list, prepended inherit
if ( !function_exists( 'pabloguadi_get_list_templates_blog' ) ) {
	function pabloguadi_get_list_templates_blog($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_templates_blog'))=='') {
			$list = pabloguadi_get_list_templates('blog');
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_templates_blog', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return blogger styles list, prepended inherit
if ( !function_exists( 'pabloguadi_get_list_templates_blogger' ) ) {
	function pabloguadi_get_list_templates_blogger($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_templates_blogger'))=='') {
			$list = pabloguadi_array_merge(pabloguadi_get_list_templates('blogger'), pabloguadi_get_list_templates('blog'));
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_templates_blogger', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return single page styles list, prepended inherit
if ( !function_exists( 'pabloguadi_get_list_templates_single' ) ) {
	function pabloguadi_get_list_templates_single($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_templates_single'))=='') {
			$list = pabloguadi_get_list_templates('single');
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_templates_single', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return header styles list, prepended inherit
if ( !function_exists( 'pabloguadi_get_list_templates_header' ) ) {
	function pabloguadi_get_list_templates_header($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_templates_header'))=='') {
			$list = pabloguadi_get_list_templates('header');
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_templates_header', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return form styles list, prepended inherit
if ( !function_exists( 'pabloguadi_get_list_templates_forms' ) ) {
	function pabloguadi_get_list_templates_forms($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_templates_forms'))=='') {
			$list = pabloguadi_get_list_templates('forms');
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_templates_forms', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return article styles list, prepended inherit
if ( !function_exists( 'pabloguadi_get_list_article_styles' ) ) {
	function pabloguadi_get_list_article_styles($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_article_styles'))=='') {
			$list = array(
				"boxed"   => esc_html__('Boxed', 'pabloguadi'),
				"stretch" => esc_html__('Stretch', 'pabloguadi')
				);
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_article_styles', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return post-formats filters list, prepended inherit
if ( !function_exists( 'pabloguadi_get_list_post_formats_filters' ) ) {
	function pabloguadi_get_list_post_formats_filters($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_post_formats_filters'))=='') {
			$list = array(
				"no"      => esc_html__('All posts', 'pabloguadi'),
				"thumbs"  => esc_html__('With thumbs', 'pabloguadi'),
				"reviews" => esc_html__('With reviews', 'pabloguadi'),
				"video"   => esc_html__('With videos', 'pabloguadi'),
				"audio"   => esc_html__('With audios', 'pabloguadi'),
				"gallery" => esc_html__('With galleries', 'pabloguadi')
				);
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_post_formats_filters', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return portfolio filters list, prepended inherit
if ( !function_exists( 'pabloguadi_get_list_portfolio_filters' ) ) {
	function pabloguadi_get_list_portfolio_filters($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_portfolio_filters'))=='') {
			$list = array(
				"hide"		=> esc_html__('Hide', 'pabloguadi'),
				"tags"		=> esc_html__('Tags', 'pabloguadi'),
				"categories"=> esc_html__('Categories', 'pabloguadi')
				);
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_portfolio_filters', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return hover styles list, prepended inherit
if ( !function_exists( 'pabloguadi_get_list_hovers' ) ) {
	function pabloguadi_get_list_hovers($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_hovers'))=='') {
			$list = array();
			$list['circle effect1']  = esc_html__('Circle Effect 1',  'pabloguadi');
			$list['circle effect2']  = esc_html__('Circle Effect 2',  'pabloguadi');
			$list['circle effect3']  = esc_html__('Circle Effect 3',  'pabloguadi');
			$list['circle effect4']  = esc_html__('Circle Effect 4',  'pabloguadi');
			$list['circle effect5']  = esc_html__('Circle Effect 5',  'pabloguadi');
			$list['circle effect6']  = esc_html__('Circle Effect 6',  'pabloguadi');
			$list['circle effect7']  = esc_html__('Circle Effect 7',  'pabloguadi');
			$list['circle effect8']  = esc_html__('Circle Effect 8',  'pabloguadi');
			$list['circle effect9']  = esc_html__('Circle Effect 9',  'pabloguadi');
			$list['circle effect10'] = esc_html__('Circle Effect 10',  'pabloguadi');
			$list['circle effect11'] = esc_html__('Circle Effect 11',  'pabloguadi');
			$list['circle effect12'] = esc_html__('Circle Effect 12',  'pabloguadi');
			$list['circle effect13'] = esc_html__('Circle Effect 13',  'pabloguadi');
			$list['circle effect14'] = esc_html__('Circle Effect 14',  'pabloguadi');
			$list['circle effect15'] = esc_html__('Circle Effect 15',  'pabloguadi');
			$list['circle effect16'] = esc_html__('Circle Effect 16',  'pabloguadi');
			$list['circle effect17'] = esc_html__('Circle Effect 17',  'pabloguadi');
			$list['circle effect18'] = esc_html__('Circle Effect 18',  'pabloguadi');
			$list['circle effect19'] = esc_html__('Circle Effect 19',  'pabloguadi');
			$list['circle effect20'] = esc_html__('Circle Effect 20',  'pabloguadi');
			$list['square effect1']  = esc_html__('Square Effect 1',  'pabloguadi');
			$list['square effect2']  = esc_html__('Square Effect 2',  'pabloguadi');
			$list['square effect3']  = esc_html__('Square Effect 3',  'pabloguadi');
			$list['square effect5']  = esc_html__('Square Effect 5',  'pabloguadi');
			$list['square effect6']  = esc_html__('Square Effect 6',  'pabloguadi');
			$list['square effect7']  = esc_html__('Square Effect 7',  'pabloguadi');
			$list['square effect8']  = esc_html__('Square Effect 8',  'pabloguadi');
			$list['square effect9']  = esc_html__('Square Effect 9',  'pabloguadi');
			$list['square effect10'] = esc_html__('Square Effect 10',  'pabloguadi');
			$list['square effect11'] = esc_html__('Square Effect 11',  'pabloguadi');
			$list['square effect12'] = esc_html__('Square Effect 12',  'pabloguadi');
			$list['square effect13'] = esc_html__('Square Effect 13',  'pabloguadi');
			$list['square effect14'] = esc_html__('Square Effect 14',  'pabloguadi');
			$list['square effect15'] = esc_html__('Square Effect 15',  'pabloguadi');
			$list['square effect_dir']   = esc_html__('Square Effect Dir',   'pabloguadi');
			$list['square effect_shift'] = esc_html__('Square Effect Shift', 'pabloguadi');
			$list['square effect_book']  = esc_html__('Square Effect Book',  'pabloguadi');
			$list['square effect_more']  = esc_html__('Square Effect More',  'pabloguadi');
			$list['square effect_fade']  = esc_html__('Square Effect Fade',  'pabloguadi');
			$list['square effect_pull']  = esc_html__('Square Effect Pull',  'pabloguadi');
			$list['square effect_slide'] = esc_html__('Square Effect Slide', 'pabloguadi');
			$list['square effect_border'] = esc_html__('Square Effect Border', 'pabloguadi');
			$list = apply_filters('pabloguadi_filter_portfolio_hovers', $list);
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_hovers', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}


// Return list of the blog counters
if ( !function_exists( 'pabloguadi_get_list_blog_counters' ) ) {
	function pabloguadi_get_list_blog_counters($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_blog_counters'))=='') {
			$list = array(
				'views'		=> esc_html__('Views', 'pabloguadi'),
				'likes'		=> esc_html__('Likes', 'pabloguadi'),
				'rating'	=> esc_html__('Rating', 'pabloguadi'),
				'comments'	=> esc_html__('Comments', 'pabloguadi')
				);
			$list = apply_filters('pabloguadi_filter_list_blog_counters', $list);
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_blog_counters', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return list of the item sizes for the portfolio alter style, prepended inherit
if ( !function_exists( 'pabloguadi_get_list_alter_sizes' ) ) {
	function pabloguadi_get_list_alter_sizes($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_alter_sizes'))=='') {
			$list = array(
					'1_1' => esc_html__('1x1', 'pabloguadi'),
					'1_2' => esc_html__('1x2', 'pabloguadi'),
					'2_1' => esc_html__('2x1', 'pabloguadi'),
					'2_2' => esc_html__('2x2', 'pabloguadi'),
					'1_3' => esc_html__('1x3', 'pabloguadi'),
					'2_3' => esc_html__('2x3', 'pabloguadi'),
					'3_1' => esc_html__('3x1', 'pabloguadi'),
					'3_2' => esc_html__('3x2', 'pabloguadi'),
					'3_3' => esc_html__('3x3', 'pabloguadi')
					);
			$list = apply_filters('pabloguadi_filter_portfolio_alter_sizes', $list);
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_alter_sizes', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return extended hover directions list, prepended inherit
if ( !function_exists( 'pabloguadi_get_list_hovers_directions' ) ) {
	function pabloguadi_get_list_hovers_directions($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_hovers_directions'))=='') {
			$list = array(
				'left_to_right' => esc_html__('Left to Right',  'pabloguadi'),
				'right_to_left' => esc_html__('Right to Left',  'pabloguadi'),
				'top_to_bottom' => esc_html__('Top to Bottom',  'pabloguadi'),
				'bottom_to_top' => esc_html__('Bottom to Top',  'pabloguadi'),
				'scale_up'      => esc_html__('Scale Up',  'pabloguadi'),
				'scale_down'    => esc_html__('Scale Down',  'pabloguadi'),
				'scale_down_up' => esc_html__('Scale Down-Up',  'pabloguadi'),
				'from_left_and_right' => esc_html__('From Left and Right',  'pabloguadi'),
				'from_top_and_bottom' => esc_html__('From Top and Bottom',  'pabloguadi')
			);
			$list = apply_filters('pabloguadi_filter_portfolio_hovers_directions', $list);
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_hovers_directions', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}


// Return list of the label positions in the custom forms
if ( !function_exists( 'pabloguadi_get_list_label_positions' ) ) {
	function pabloguadi_get_list_label_positions($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_label_positions'))=='') {
			$list = array(
				'top'		=> esc_html__('Top',		'pabloguadi'),
				'bottom'	=> esc_html__('Bottom',		'pabloguadi'),
				'left'		=> esc_html__('Left',		'pabloguadi'),
				'over'		=> esc_html__('Over',		'pabloguadi')
			);
			$list = apply_filters('pabloguadi_filter_label_positions', $list);
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_label_positions', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}


// Return list of the bg image positions
if ( !function_exists( 'pabloguadi_get_list_bg_image_positions' ) ) {
	function pabloguadi_get_list_bg_image_positions($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_bg_image_positions'))=='') {
			$list = array(
				'left top'	   => esc_html__('Left Top', 'pabloguadi'),
				'center top'   => esc_html__("Center Top", 'pabloguadi'),
				'right top'    => esc_html__("Right Top", 'pabloguadi'),
				'left center'  => esc_html__("Left Center", 'pabloguadi'),
				'center center'=> esc_html__("Center Center", 'pabloguadi'),
				'right center' => esc_html__("Right Center", 'pabloguadi'),
				'left bottom'  => esc_html__("Left Bottom", 'pabloguadi'),
				'center bottom'=> esc_html__("Center Bottom", 'pabloguadi'),
				'right bottom' => esc_html__("Right Bottom", 'pabloguadi')
			);
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_bg_image_positions', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}


// Return list of the bg image repeat
if ( !function_exists( 'pabloguadi_get_list_bg_image_repeats' ) ) {
	function pabloguadi_get_list_bg_image_repeats($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_bg_image_repeats'))=='') {
			$list = array(
				'repeat'	=> esc_html__('Repeat', 'pabloguadi'),
				'repeat-x'	=> esc_html__('Repeat X', 'pabloguadi'),
				'repeat-y'	=> esc_html__('Repeat Y', 'pabloguadi'),
				'no-repeat'	=> esc_html__('No Repeat', 'pabloguadi')
			);
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_bg_image_repeats', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}


// Return list of the bg image attachment
if ( !function_exists( 'pabloguadi_get_list_bg_image_attachments' ) ) {
	function pabloguadi_get_list_bg_image_attachments($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_bg_image_attachments'))=='') {
			$list = array(
				'scroll'	=> esc_html__('Scroll', 'pabloguadi'),
				'fixed'		=> esc_html__('Fixed', 'pabloguadi'),
				'local'		=> esc_html__('Local', 'pabloguadi')
			);
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_bg_image_attachments', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}


// Return list of the bg tints
if ( !function_exists( 'pabloguadi_get_list_bg_tints' ) ) {
	function pabloguadi_get_list_bg_tints($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_bg_tints'))=='') {
			$list = array(
				'white'	=> esc_html__('White', 'pabloguadi'),
				'light'	=> esc_html__('Light', 'pabloguadi'),
				'dark'	=> esc_html__('Dark', 'pabloguadi')
			);
			$list = apply_filters('pabloguadi_filter_bg_tints', $list);
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_bg_tints', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return custom fields types list, prepended inherit
if ( !function_exists( 'pabloguadi_get_list_field_types' ) ) {
	function pabloguadi_get_list_field_types($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_field_types'))=='') {
			$list = array(
				'text'     => esc_html__('Text',  'pabloguadi'),
				'textarea' => esc_html__('Text Area','pabloguadi'),
				'password' => esc_html__('Password',  'pabloguadi'),
				'radio'    => esc_html__('Radio',  'pabloguadi'),
				'checkbox' => esc_html__('Checkbox',  'pabloguadi'),
				'select'   => esc_html__('Select',  'pabloguadi'),
				'date'     => esc_html__('Date','pabloguadi'),
				'time'     => esc_html__('Time','pabloguadi'),
				'button'   => esc_html__('Button','pabloguadi')
			);
			$list = apply_filters('pabloguadi_filter_field_types', $list);
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_field_types', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return Google map styles
if ( !function_exists( 'pabloguadi_get_list_googlemap_styles' ) ) {
	function pabloguadi_get_list_googlemap_styles($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_googlemap_styles'))=='') {
			$list = array(
				'default' => esc_html__('Default', 'pabloguadi')
			);
			$list = apply_filters('pabloguadi_filter_googlemap_styles', $list);
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_googlemap_styles', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return images list
if (!function_exists('pabloguadi_get_list_images')) {
	function pabloguadi_get_list_images($folder, $ext='', $only_names=false) {
		return function_exists('trx_utils_get_folder_list') ? trx_utils_get_folder_list($folder, $ext, $only_names) : array();
	}
}

// Return iconed classes list
if ( !function_exists( 'pabloguadi_get_list_icons' ) ) {
	function pabloguadi_get_list_icons($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_icons'))=='') {
			$list = pabloguadi_parse_icons_classes(pabloguadi_get_file_dir("css/fontello/css/fontello-codes.css"));
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_icons', $list);
		}
		return $prepend_inherit ? array_merge(array('inherit'), $list) : $list;
	}
}

// Return socials list
if ( !function_exists( 'pabloguadi_get_list_socials' ) ) {
	function pabloguadi_get_list_socials($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_socials'))=='') {
			$list = pabloguadi_get_list_images("images/socials", "png");
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_socials', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return list with 'Yes' and 'No' items
if ( !function_exists( 'pabloguadi_get_list_yesno' ) ) {
	function pabloguadi_get_list_yesno($prepend_inherit=false) {
		$list = array(
			'yes' => esc_html__("Yes", 'pabloguadi'),
			'no'  => esc_html__("No", 'pabloguadi')
		);
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return list with 'On' and 'Of' items
if ( !function_exists( 'pabloguadi_get_list_onoff' ) ) {
	function pabloguadi_get_list_onoff($prepend_inherit=false) {
		$list = array(
			"on" => esc_html__("On", 'pabloguadi'),
			"off" => esc_html__("Off", 'pabloguadi')
		);
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return list with 'Show' and 'Hide' items
if ( !function_exists( 'pabloguadi_get_list_showhide' ) ) {
	function pabloguadi_get_list_showhide($prepend_inherit=false) {
		$list = array(
			"show" => esc_html__("Show", 'pabloguadi'),
			"hide" => esc_html__("Hide", 'pabloguadi')
		);
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return list with 'Ascending' and 'Descending' items
if ( !function_exists( 'pabloguadi_get_list_orderings' ) ) {
	function pabloguadi_get_list_orderings($prepend_inherit=false) {
		$list = array(
			"asc" => esc_html__("Ascending", 'pabloguadi'),
			"desc" => esc_html__("Descending", 'pabloguadi')
		);
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return list with 'Horizontal' and 'Vertical' items
if ( !function_exists( 'pabloguadi_get_list_directions' ) ) {
	function pabloguadi_get_list_directions($prepend_inherit=false) {
		$list = array(
			"horizontal" => esc_html__("Horizontal", 'pabloguadi'),
			"vertical" => esc_html__("Vertical", 'pabloguadi')
		);
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return list with item's shapes
if ( !function_exists( 'pabloguadi_get_list_shapes' ) ) {
	function pabloguadi_get_list_shapes($prepend_inherit=false) {
		$list = array(
			"round"  => esc_html__("Round", 'pabloguadi'),
			"square" => esc_html__("Square", 'pabloguadi')
		);
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return list with item's sizes
if ( !function_exists( 'pabloguadi_get_list_sizes' ) ) {
	function pabloguadi_get_list_sizes($prepend_inherit=false) {
		$list = array(
			"tiny"   => esc_html__("Tiny", 'pabloguadi'),
			"small"  => esc_html__("Small", 'pabloguadi'),
			"medium" => esc_html__("Medium", 'pabloguadi'),
			"large"  => esc_html__("Large", 'pabloguadi')
		);
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return list with slider (scroll) controls positions
if ( !function_exists( 'pabloguadi_get_list_controls' ) ) {
	function pabloguadi_get_list_controls($prepend_inherit=false) {
		$list = array(
			"hide" => esc_html__("Hide", 'pabloguadi'),
			"side" => esc_html__("Side", 'pabloguadi'),
			"bottom" => esc_html__("Bottom", 'pabloguadi')
		);
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return list with float items
if ( !function_exists( 'pabloguadi_get_list_floats' ) ) {
	function pabloguadi_get_list_floats($prepend_inherit=false) {
		$list = array(
			"none" => esc_html__("None", 'pabloguadi'),
			"left" => esc_html__("Float Left", 'pabloguadi'),
			"right" => esc_html__("Float Right", 'pabloguadi')
		);
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return list with alignment items
if ( !function_exists( 'pabloguadi_get_list_alignments' ) ) {
	function pabloguadi_get_list_alignments($justify=false, $prepend_inherit=false) {
		$list = array(
			"none" => esc_html__("None", 'pabloguadi'),
			"left" => esc_html__("Left", 'pabloguadi'),
			"center" => esc_html__("Center", 'pabloguadi'),
			"right" => esc_html__("Right", 'pabloguadi')
		);
		if ($justify) $list["justify"] = esc_html__("Justify", 'pabloguadi');
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return list with horizontal positions
if ( !function_exists( 'pabloguadi_get_list_hpos' ) ) {
	function pabloguadi_get_list_hpos($prepend_inherit=false, $center=false) {
		$list = array();
		$list['left'] = esc_html__("Left", 'pabloguadi');
		if ($center) $list['center'] = esc_html__("Center", 'pabloguadi');
		$list['right'] = esc_html__("Right", 'pabloguadi');
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return list with vertical positions
if ( !function_exists( 'pabloguadi_get_list_vpos' ) ) {
	function pabloguadi_get_list_vpos($prepend_inherit=false, $center=false) {
		$list = array();
		$list['top'] = esc_html__("Top", 'pabloguadi');
		if ($center) $list['center'] = esc_html__("Center", 'pabloguadi');
		$list['bottom'] = esc_html__("Bottom", 'pabloguadi');
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return sorting list items
if ( !function_exists( 'pabloguadi_get_list_sortings' ) ) {
	function pabloguadi_get_list_sortings($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_sortings'))=='') {
			$list = array(
				"date" => esc_html__("Date", 'pabloguadi'),
				"title" => esc_html__("Alphabetically", 'pabloguadi'),
				"views" => esc_html__("Popular (views count)", 'pabloguadi'),
				"comments" => esc_html__("Most commented (comments count)", 'pabloguadi'),
				"author_rating" => esc_html__("Author rating", 'pabloguadi'),
				"users_rating" => esc_html__("Visitors (users) rating", 'pabloguadi'),
				"random" => esc_html__("Random", 'pabloguadi')
			);
			$list = apply_filters('pabloguadi_filter_list_sortings', $list);
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_sortings', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return list with columns widths
if ( !function_exists( 'pabloguadi_get_list_columns' ) ) {
	function pabloguadi_get_list_columns($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_columns'))=='') {
			$list = array(
				"none" => esc_html__("None", 'pabloguadi'),
				"1_1" => esc_html__("100%", 'pabloguadi'),
				"1_2" => esc_html__("1/2", 'pabloguadi'),
				"1_3" => esc_html__("1/3", 'pabloguadi'),
				"2_3" => esc_html__("2/3", 'pabloguadi'),
				"1_4" => esc_html__("1/4", 'pabloguadi'),
				"3_4" => esc_html__("3/4", 'pabloguadi'),
				"1_5" => esc_html__("1/5", 'pabloguadi'),
				"2_5" => esc_html__("2/5", 'pabloguadi'),
				"3_5" => esc_html__("3/5", 'pabloguadi'),
				"4_5" => esc_html__("4/5", 'pabloguadi'),
				"1_6" => esc_html__("1/6", 'pabloguadi'),
				"5_6" => esc_html__("5/6", 'pabloguadi'),
				"1_7" => esc_html__("1/7", 'pabloguadi'),
				"2_7" => esc_html__("2/7", 'pabloguadi'),
				"3_7" => esc_html__("3/7", 'pabloguadi'),
				"4_7" => esc_html__("4/7", 'pabloguadi'),
				"5_7" => esc_html__("5/7", 'pabloguadi'),
				"6_7" => esc_html__("6/7", 'pabloguadi'),
				"1_8" => esc_html__("1/8", 'pabloguadi'),
				"3_8" => esc_html__("3/8", 'pabloguadi'),
				"5_8" => esc_html__("5/8", 'pabloguadi'),
				"7_8" => esc_html__("7/8", 'pabloguadi'),
				"1_9" => esc_html__("1/9", 'pabloguadi'),
				"2_9" => esc_html__("2/9", 'pabloguadi'),
				"4_9" => esc_html__("4/9", 'pabloguadi'),
				"5_9" => esc_html__("5/9", 'pabloguadi'),
				"7_9" => esc_html__("7/9", 'pabloguadi'),
				"8_9" => esc_html__("8/9", 'pabloguadi'),
				"1_10"=> esc_html__("1/10", 'pabloguadi'),
				"3_10"=> esc_html__("3/10", 'pabloguadi'),
				"7_10"=> esc_html__("7/10", 'pabloguadi'),
				"9_10"=> esc_html__("9/10", 'pabloguadi'),
				"1_11"=> esc_html__("1/11", 'pabloguadi'),
				"2_11"=> esc_html__("2/11", 'pabloguadi'),
				"3_11"=> esc_html__("3/11", 'pabloguadi'),
				"4_11"=> esc_html__("4/11", 'pabloguadi'),
				"5_11"=> esc_html__("5/11", 'pabloguadi'),
				"6_11"=> esc_html__("6/11", 'pabloguadi'),
				"7_11"=> esc_html__("7/11", 'pabloguadi'),
				"8_11"=> esc_html__("8/11", 'pabloguadi'),
				"9_11"=> esc_html__("9/11", 'pabloguadi'),
				"10_11"=> esc_html__("10/11", 'pabloguadi'),
				"1_12"=> esc_html__("1/12", 'pabloguadi'),
				"5_12"=> esc_html__("5/12", 'pabloguadi'),
				"7_12"=> esc_html__("7/12", 'pabloguadi'),
				"10_12"=> esc_html__("10/12", 'pabloguadi'),
				"11_12"=> esc_html__("11/12", 'pabloguadi')
			);
			$list = apply_filters('pabloguadi_filter_list_columns', $list);
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_columns', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return list of locations for the dedicated content
if ( !function_exists( 'pabloguadi_get_list_dedicated_locations' ) ) {
	function pabloguadi_get_list_dedicated_locations($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_dedicated_locations'))=='') {
			$list = array(
				"default" => esc_html__('As in the post defined', 'pabloguadi'),
				"center"  => esc_html__('Above the text of the post', 'pabloguadi'),
				"left"    => esc_html__('To the left the text of the post', 'pabloguadi'),
				"right"   => esc_html__('To the right the text of the post', 'pabloguadi'),
				"alter"   => esc_html__('Alternates for each post', 'pabloguadi')
			);
			$list = apply_filters('pabloguadi_filter_list_dedicated_locations', $list);
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_dedicated_locations', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return post-format name
if ( !function_exists( 'pabloguadi_get_post_format_name' ) ) {
	function pabloguadi_get_post_format_name($format, $single=true) {
		$name = '';
		if ($format=='gallery')		$name = $single ? esc_html__('gallery', 'pabloguadi') : esc_html__('galleries', 'pabloguadi');
		else if ($format=='video')	$name = $single ? esc_html__('video', 'pabloguadi') : esc_html__('videos', 'pabloguadi');
		else if ($format=='audio')	$name = $single ? esc_html__('audio', 'pabloguadi') : esc_html__('audios', 'pabloguadi');
		else if ($format=='image')	$name = $single ? esc_html__('image', 'pabloguadi') : esc_html__('images', 'pabloguadi');
		else if ($format=='quote')	$name = $single ? esc_html__('quote', 'pabloguadi') : esc_html__('quotes', 'pabloguadi');
		else if ($format=='link')	$name = $single ? esc_html__('link', 'pabloguadi') : esc_html__('links', 'pabloguadi');
		else if ($format=='status')	$name = $single ? esc_html__('status', 'pabloguadi') : esc_html__('statuses', 'pabloguadi');
		else if ($format=='aside')	$name = $single ? esc_html__('aside', 'pabloguadi') : esc_html__('asides', 'pabloguadi');
		else if ($format=='chat')	$name = $single ? esc_html__('chat', 'pabloguadi') : esc_html__('chats', 'pabloguadi');
		else						$name = $single ? esc_html__('standard', 'pabloguadi') : esc_html__('standards', 'pabloguadi');
		return apply_filters('pabloguadi_filter_list_post_format_name', $name, $format);
	}
}

// Return post-format icon name (from Fontello library)
if ( !function_exists( 'pabloguadi_get_post_format_icon' ) ) {
	function pabloguadi_get_post_format_icon($format) {
		$icon = 'icon-';
		if ($format=='gallery')		$icon .= 'pictures';
		else if ($format=='video')	$icon .= 'video';
		else if ($format=='audio')	$icon .= 'note';
		else if ($format=='image')	$icon .= 'picture';
		else if ($format=='quote')	$icon .= 'quote';
		else if ($format=='link')	$icon .= 'link';
		else if ($format=='status')	$icon .= 'comment';
		else if ($format=='aside')	$icon .= 'doc-text';
		else if ($format=='chat')	$icon .= 'chat';
		else						$icon .= 'book-open';
		return apply_filters('pabloguadi_filter_list_post_format_icon', $icon, $format);
	}
}

// Return fonts styles list, prepended inherit
if ( !function_exists( 'pabloguadi_get_list_fonts_styles' ) ) {
	function pabloguadi_get_list_fonts_styles($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_fonts_styles'))=='') {
			$list = array(
				'i' => esc_html__('I','pabloguadi'),
				'u' => esc_html__('U', 'pabloguadi')
			);
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_fonts_styles', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return Google fonts list
if ( !function_exists( 'pabloguadi_get_list_fonts' ) ) {
	function pabloguadi_get_list_fonts($prepend_inherit=false) {
		if (($list = pabloguadi_storage_get('list_fonts'))=='') {
			$list = array();
			$list = pabloguadi_array_merge($list, pabloguadi_get_list_font_faces());
			// Google and custom fonts list:
			//$list['Advent Pro'] = array(
			//		'family'=>'sans-serif',																						// (required) font family
			//		'link'=>'Advent+Pro:100,100italic,300,300italic,400,400italic,500,500italic,700,700italic,900,900italic',	// (optional) if you use Google font repository
			//		'css'=>pabloguadi_get_file_url('/css/font-face/Advent-Pro/stylesheet.css')									// (optional) if you use custom font-face
			//		);
			$list = pabloguadi_array_merge($list, array(
				'Advent Pro' => array('family'=>'sans-serif'),
				'Alegreya Sans' => array('family'=>'sans-serif'),
				'Arimo' => array('family'=>'sans-serif'),
				'Asap' => array('family'=>'sans-serif'),
				'Averia Sans Libre' => array('family'=>'cursive'),
				'Averia Serif Libre' => array('family'=>'cursive'),
				'Bree Serif' => array('family'=>'serif',),
				'Cabin' => array('family'=>'sans-serif'),
				'Cabin Condensed' => array('family'=>'sans-serif'),
				'Caudex' => array('family'=>'serif'),
				'Comfortaa' => array('family'=>'cursive'),
				'Cousine' => array('family'=>'sans-serif'),
				'Crimson Text' => array('family'=>'serif'),
				'Cuprum' => array('family'=>'sans-serif'),
				'Dosis' => array('family'=>'sans-serif'),
				'Economica' => array('family'=>'sans-serif'),
				'Exo' => array('family'=>'sans-serif'),
				'Expletus Sans' => array('family'=>'cursive'),
				'Karla' => array('family'=>'sans-serif'),
				'Lato' => array('family'=>'sans-serif'),
				'Lekton' => array('family'=>'sans-serif'),
				'Lobster Two' => array('family'=>'cursive'),
				'Maven Pro' => array('family'=>'sans-serif'),
				'Merriweather' => array('family'=>'serif'),
				'Montserrat' => array('family'=>'sans-serif'),
				'Neuton' => array('family'=>'serif'),
				'Noticia Text' => array('family'=>'serif'),
				'Old Standard TT' => array('family'=>'serif'),
				'Open Sans' => array('family'=>'sans-serif'),
				'Orbitron' => array('family'=>'sans-serif'),
				'Oswald' => array('family'=>'sans-serif'),
				'Overlock' => array('family'=>'cursive'),
				'Oxygen' => array('family'=>'sans-serif'),
				'Philosopher' => array('family'=>'serif'),
				'PT Serif' => array('family'=>'serif'),
				'Puritan' => array('family'=>'sans-serif'),
				'Raleway' => array('family'=>'sans-serif'),
				'Roboto' => array('family'=>'sans-serif'),
				'Roboto Slab' => array('family'=>'sans-serif'),
				'Roboto Condensed' => array('family'=>'sans-serif'),
				'Rosario' => array('family'=>'sans-serif'),
				'Share' => array('family'=>'cursive'),
				'Signika' => array('family'=>'sans-serif'),
				'Signika Negative' => array('family'=>'sans-serif'),
				'Source Sans Pro' => array('family'=>'sans-serif'),
				'Tinos' => array('family'=>'serif'),
				'Ubuntu' => array('family'=>'sans-serif'),
				'Vollkorn' => array('family'=>'serif')
				)
			);
			$list = apply_filters('pabloguadi_filter_list_fonts', $list);
			if (pabloguadi_get_theme_setting('use_list_cache')) pabloguadi_storage_set('list_fonts', $list);
		}
		return $prepend_inherit ? pabloguadi_array_merge(array('inherit' => esc_html__("Inherit", 'pabloguadi')), $list) : $list;
	}
}

// Return Custom font-face list
if ( !function_exists( 'pabloguadi_get_list_font_faces' ) ) {
	function pabloguadi_get_list_font_faces($prepend_inherit=false) {
		static $list = false;
		if (is_array($list)) return $list;
		$fonts = pabloguadi_storage_get('required_custom_fonts');
		$list = array();
		if (is_array($fonts)) {
			foreach ($fonts as $font) {
				if (($url = pabloguadi_get_file_url('css/font-face/'.trim($font).'/stylesheet.css'))!='') {
					$list[sprintf(esc_html__('%s (uploaded font)', 'pabloguadi'), $font)] = array('css' => $url);
				}
			}
		}
		return $list;
	}
}
?>