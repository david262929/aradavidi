<?php
/**
 * PabloGuadi Framework: theme variables storage
 *
 * @package	pabloguadi
 * @since	pabloguadi 1.0
 */

// Disable direct call
if ( ! defined( 'ABSPATH' ) ) { exit; }

// Get theme variable
if (!function_exists('pabloguadi_storage_get')) {
	function pabloguadi_storage_get($var_name, $default='') {
		global $PABLOGUADI_STORAGE;
		return isset($PABLOGUADI_STORAGE[$var_name]) ? $PABLOGUADI_STORAGE[$var_name] : $default;
	}
}

// Set theme variable
if (!function_exists('pabloguadi_storage_set')) {
	function pabloguadi_storage_set($var_name, $value) {
		global $PABLOGUADI_STORAGE;
		$PABLOGUADI_STORAGE[$var_name] = $value;
	}
}

// Check if theme variable is empty
if (!function_exists('pabloguadi_storage_empty')) {
	function pabloguadi_storage_empty($var_name, $key='', $key2='') {
		global $PABLOGUADI_STORAGE;
		if (!empty($key) && !empty($key2))
			return empty($PABLOGUADI_STORAGE[$var_name][$key][$key2]);
		else if (!empty($key))
			return empty($PABLOGUADI_STORAGE[$var_name][$key]);
		else
			return empty($PABLOGUADI_STORAGE[$var_name]);
	}
}

// Check if theme variable is set
if (!function_exists('pabloguadi_storage_isset')) {
	function pabloguadi_storage_isset($var_name, $key='', $key2='') {
		global $PABLOGUADI_STORAGE;
		if (!empty($key) && !empty($key2))
			return isset($PABLOGUADI_STORAGE[$var_name][$key][$key2]);
		else if (!empty($key))
			return isset($PABLOGUADI_STORAGE[$var_name][$key]);
		else
			return isset($PABLOGUADI_STORAGE[$var_name]);
	}
}

// Inc/Dec theme variable with specified value
if (!function_exists('pabloguadi_storage_inc')) {
	function pabloguadi_storage_inc($var_name, $value=1) {
		global $PABLOGUADI_STORAGE;
		if (empty($PABLOGUADI_STORAGE[$var_name])) $PABLOGUADI_STORAGE[$var_name] = 0;
		$PABLOGUADI_STORAGE[$var_name] += $value;
	}
}

// Concatenate theme variable with specified value
if (!function_exists('pabloguadi_storage_concat')) {
	function pabloguadi_storage_concat($var_name, $value) {
		global $PABLOGUADI_STORAGE;
		if (empty($PABLOGUADI_STORAGE[$var_name])) $PABLOGUADI_STORAGE[$var_name] = '';
		$PABLOGUADI_STORAGE[$var_name] .= $value;
	}
}

// Get array (one or two dim) element
if (!function_exists('pabloguadi_storage_get_array')) {
	function pabloguadi_storage_get_array($var_name, $key, $key2='', $default='') {
		global $PABLOGUADI_STORAGE;
		if (empty($key2))
			return !empty($var_name) && !empty($key) && isset($PABLOGUADI_STORAGE[$var_name][$key]) ? $PABLOGUADI_STORAGE[$var_name][$key] : $default;
		else
			return !empty($var_name) && !empty($key) && isset($PABLOGUADI_STORAGE[$var_name][$key][$key2]) ? $PABLOGUADI_STORAGE[$var_name][$key][$key2] : $default;
	}
}

// Set array element
if (!function_exists('pabloguadi_storage_set_array')) {
	function pabloguadi_storage_set_array($var_name, $key, $value) {
		global $PABLOGUADI_STORAGE;
		if (!isset($PABLOGUADI_STORAGE[$var_name])) $PABLOGUADI_STORAGE[$var_name] = array();
		if ($key==='')
			$PABLOGUADI_STORAGE[$var_name][] = $value;
		else
			$PABLOGUADI_STORAGE[$var_name][$key] = $value;
	}
}

// Set two-dim array element
if (!function_exists('pabloguadi_storage_set_array2')) {
	function pabloguadi_storage_set_array2($var_name, $key, $key2, $value) {
		global $PABLOGUADI_STORAGE;
		if (!isset($PABLOGUADI_STORAGE[$var_name])) $PABLOGUADI_STORAGE[$var_name] = array();
		if (!isset($PABLOGUADI_STORAGE[$var_name][$key])) $PABLOGUADI_STORAGE[$var_name][$key] = array();
		if ($key2==='')
			$PABLOGUADI_STORAGE[$var_name][$key][] = $value;
		else
			$PABLOGUADI_STORAGE[$var_name][$key][$key2] = $value;
	}
}

// Add array element after the key
if (!function_exists('pabloguadi_storage_set_array_after')) {
	function pabloguadi_storage_set_array_after($var_name, $after, $key, $value='') {
		global $PABLOGUADI_STORAGE;
		if (!isset($PABLOGUADI_STORAGE[$var_name])) $PABLOGUADI_STORAGE[$var_name] = array();
		if (is_array($key))
			pabloguadi_array_insert_after($PABLOGUADI_STORAGE[$var_name], $after, $key);
		else
			pabloguadi_array_insert_after($PABLOGUADI_STORAGE[$var_name], $after, array($key=>$value));
	}
}

// Add array element before the key
if (!function_exists('pabloguadi_storage_set_array_before')) {
	function pabloguadi_storage_set_array_before($var_name, $before, $key, $value='') {
		global $PABLOGUADI_STORAGE;
		if (!isset($PABLOGUADI_STORAGE[$var_name])) $PABLOGUADI_STORAGE[$var_name] = array();
		if (is_array($key))
			pabloguadi_array_insert_before($PABLOGUADI_STORAGE[$var_name], $before, $key);
		else
			pabloguadi_array_insert_before($PABLOGUADI_STORAGE[$var_name], $before, array($key=>$value));
	}
}

// Push element into array
if (!function_exists('pabloguadi_storage_push_array')) {
	function pabloguadi_storage_push_array($var_name, $key, $value) {
		global $PABLOGUADI_STORAGE;
		if (!isset($PABLOGUADI_STORAGE[$var_name])) $PABLOGUADI_STORAGE[$var_name] = array();
		if ($key==='')
			array_push($PABLOGUADI_STORAGE[$var_name], $value);
		else {
			if (!isset($PABLOGUADI_STORAGE[$var_name][$key])) $PABLOGUADI_STORAGE[$var_name][$key] = array();
			array_push($PABLOGUADI_STORAGE[$var_name][$key], $value);
		}
	}
}

// Pop element from array
if (!function_exists('pabloguadi_storage_pop_array')) {
	function pabloguadi_storage_pop_array($var_name, $key='', $defa='') {
		global $PABLOGUADI_STORAGE;
		$rez = $defa;
		if ($key==='') {
			if (isset($PABLOGUADI_STORAGE[$var_name]) && is_array($PABLOGUADI_STORAGE[$var_name]) && count($PABLOGUADI_STORAGE[$var_name]) > 0) 
				$rez = array_pop($PABLOGUADI_STORAGE[$var_name]);
		} else {
			if (isset($PABLOGUADI_STORAGE[$var_name][$key]) && is_array($PABLOGUADI_STORAGE[$var_name][$key]) && count($PABLOGUADI_STORAGE[$var_name][$key]) > 0) 
				$rez = array_pop($PABLOGUADI_STORAGE[$var_name][$key]);
		}
		return $rez;
	}
}

// Inc/Dec array element with specified value
if (!function_exists('pabloguadi_storage_inc_array')) {
	function pabloguadi_storage_inc_array($var_name, $key, $value=1) {
		global $PABLOGUADI_STORAGE;
		if (!isset($PABLOGUADI_STORAGE[$var_name])) $PABLOGUADI_STORAGE[$var_name] = array();
		if (empty($PABLOGUADI_STORAGE[$var_name][$key])) $PABLOGUADI_STORAGE[$var_name][$key] = 0;
		$PABLOGUADI_STORAGE[$var_name][$key] += $value;
	}
}

// Concatenate array element with specified value
if (!function_exists('pabloguadi_storage_concat_array')) {
	function pabloguadi_storage_concat_array($var_name, $key, $value) {
		global $PABLOGUADI_STORAGE;
		if (!isset($PABLOGUADI_STORAGE[$var_name])) $PABLOGUADI_STORAGE[$var_name] = array();
		if (empty($PABLOGUADI_STORAGE[$var_name][$key])) $PABLOGUADI_STORAGE[$var_name][$key] = '';
		$PABLOGUADI_STORAGE[$var_name][$key] .= $value;
	}
}

// Call object's method
if (!function_exists('pabloguadi_storage_call_obj_method')) {
	function pabloguadi_storage_call_obj_method($var_name, $method, $param=null) {
		global $PABLOGUADI_STORAGE;
		if ($param===null)
			return !empty($var_name) && !empty($method) && isset($PABLOGUADI_STORAGE[$var_name]) ? $PABLOGUADI_STORAGE[$var_name]->$method(): '';
		else
			return !empty($var_name) && !empty($method) && isset($PABLOGUADI_STORAGE[$var_name]) ? $PABLOGUADI_STORAGE[$var_name]->$method($param): '';
	}
}

// Get object's property
if (!function_exists('pabloguadi_storage_get_obj_property')) {
	function pabloguadi_storage_get_obj_property($var_name, $prop, $default='') {
		global $PABLOGUADI_STORAGE;
		return !empty($var_name) && !empty($prop) && isset($PABLOGUADI_STORAGE[$var_name]->$prop) ? $PABLOGUADI_STORAGE[$var_name]->$prop : $default;
	}
}
?>