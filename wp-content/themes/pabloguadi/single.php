<?php
/**
 * Single post
 */
get_header(); 

$single_style = pabloguadi_storage_get('single_style');
if (empty($single_style)) $single_style = pabloguadi_get_custom_option('single_style');

while ( have_posts() ) { the_post();
	pabloguadi_show_post_layout(
		array(
			'layout' => $single_style,
			'sidebar' => !pabloguadi_param_is_off(pabloguadi_get_custom_option('show_sidebar_main')),
			'content' => pabloguadi_get_template_property($single_style, 'need_content'),
			'terms_list' => pabloguadi_get_template_property($single_style, 'need_terms')
		)
	);
}

get_footer();
?>