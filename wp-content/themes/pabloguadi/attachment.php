<?php
/**
 * Attachment page
 */
get_header(); 

while ( have_posts() ) { the_post();

	// Move pabloguadi_set_post_views to the javascript - counter will work under cache system
	if (pabloguadi_get_custom_option('use_ajax_views_counter')=='no') {
		pabloguadi_set_post_views(get_the_ID());
	}

	pabloguadi_show_post_layout(
		array(
			'layout' => 'attachment',
			'sidebar' => !pabloguadi_param_is_off(pabloguadi_get_custom_option('show_sidebar_main'))
		)
	);

}

get_footer();
?>