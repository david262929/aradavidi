<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('pabloguadi_sc_anchor_theme_setup')) {
	add_action( 'pabloguadi_action_before_init_theme', 'pabloguadi_sc_anchor_theme_setup' );
	function pabloguadi_sc_anchor_theme_setup() {
		add_action('pabloguadi_action_shortcodes_list', 		'pabloguadi_sc_anchor_reg_shortcodes');
		if (function_exists('pabloguadi_exists_visual_composer') && pabloguadi_exists_visual_composer())
			add_action('pabloguadi_action_shortcodes_list_vc','pabloguadi_sc_anchor_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_anchor id="unique_id" description="Anchor description" title="Short Caption" icon="icon-class"]
*/

if (!function_exists('pabloguadi_sc_anchor')) {	
	function pabloguadi_sc_anchor($atts, $content = null) {
		if (pabloguadi_in_shortcode_blogger()) return '';
		extract(pabloguadi_html_decode(shortcode_atts(array(
			// Individual params
			"title" => "",
			"description" => '',
			"icon" => '',
			"url" => "",
			"separator" => "no",
			// Common params
			"id" => ""
		), $atts)));
		$output = $id 
			? '<a id="'.esc_attr($id).'"'
				. ' class="sc_anchor"' 
				. ' title="' . ($title ? esc_attr($title) : '') . '"'
				. ' data-description="' . ($description ? esc_attr(pabloguadi_strmacros($description)) : ''). '"'
				. ' data-icon="' . ($icon ? $icon : '') . '"' 
				. ' data-url="' . ($url ? esc_attr($url) : '') . '"' 
				. ' data-separator="' . (pabloguadi_param_is_on($separator) ? 'yes' : 'no') . '"'
				. '></a>'
			: '';
		return apply_filters('pabloguadi_shortcode_output', $output, 'trx_anchor', $atts, $content);
	}
	pabloguadi_require_shortcode("trx_anchor", "pabloguadi_sc_anchor");
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'pabloguadi_sc_anchor_reg_shortcodes' ) ) {
	//add_action('pabloguadi_action_shortcodes_list', 'pabloguadi_sc_anchor_reg_shortcodes');
	function pabloguadi_sc_anchor_reg_shortcodes() {
	
		pabloguadi_sc_map("trx_anchor", array(
			"title" => esc_html__("Anchor", 'pabloguadi'),
			"desc" => wp_kses_data( __("Insert anchor for the TOC (table of content)", 'pabloguadi') ),
			"decorate" => false,
			"container" => false,
			"params" => array(
				"icon" => array(
					"title" => esc_html__("Anchor's icon",  'pabloguadi'),
					"desc" => wp_kses_data( __('Select icon for the anchor from Fontello icons set',  'pabloguadi') ),
					"value" => "",
					"type" => "icons",
					"options" => pabloguadi_get_sc_param('icons')
				),
				"title" => array(
					"title" => esc_html__("Short title", 'pabloguadi'),
					"desc" => wp_kses_data( __("Short title of the anchor (for the table of content)", 'pabloguadi') ),
					"value" => "",
					"type" => "text"
				),
				"description" => array(
					"title" => esc_html__("Long description", 'pabloguadi'),
					"desc" => wp_kses_data( __("Description for the popup (then hover on the icon). You can use:<br>'{{' and '}}' - to make the text italic,<br>'((' and '))' - to make the text bold,<br>'||' - to insert line break", 'pabloguadi') ),
					"value" => "",
					"type" => "text"
				),
				"url" => array(
					"title" => esc_html__("External URL", 'pabloguadi'),
					"desc" => wp_kses_data( __("External URL for this TOC item", 'pabloguadi') ),
					"value" => "",
					"type" => "text"
				),
				"separator" => array(
					"title" => esc_html__("Add separator", 'pabloguadi'),
					"desc" => wp_kses_data( __("Add separator under item in the TOC", 'pabloguadi') ),
					"value" => "no",
					"type" => "switch",
					"options" => pabloguadi_get_sc_param('yes_no')
				),
				"id" => pabloguadi_get_sc_param('id')
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'pabloguadi_sc_anchor_reg_shortcodes_vc' ) ) {
	//add_action('pabloguadi_action_shortcodes_list_vc', 'pabloguadi_sc_anchor_reg_shortcodes_vc');
	function pabloguadi_sc_anchor_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_anchor",
			"name" => esc_html__("Anchor", 'pabloguadi'),
			"description" => wp_kses_data( __("Insert anchor for the TOC (table of content)", 'pabloguadi') ),
			"category" => esc_html__('Content', 'pabloguadi'),
			'icon' => 'icon_trx_anchor',
			"class" => "trx_sc_single trx_sc_anchor",
			"content_element" => true,
			"is_container" => false,
			"show_settings_on_create" => true,
			"params" => array(
				array(
					"param_name" => "icon",
					"heading" => esc_html__("Anchor's icon", 'pabloguadi'),
					"description" => wp_kses_data( __("Select icon for the anchor from Fontello icons set", 'pabloguadi') ),
					"class" => "",
					"value" => pabloguadi_get_sc_param('icons'),
					"type" => "dropdown"
				),
				array(
					"param_name" => "title",
					"heading" => esc_html__("Short title", 'pabloguadi'),
					"description" => wp_kses_data( __("Short title of the anchor (for the table of content)", 'pabloguadi') ),
					"admin_label" => true,
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "description",
					"heading" => esc_html__("Long description", 'pabloguadi'),
					"description" => wp_kses_data( __("Description for the popup (then hover on the icon). You can use:<br>'{{' and '}}' - to make the text italic,<br>'((' and '))' - to make the text bold,<br>'||' - to insert line break", 'pabloguadi') ),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "url",
					"heading" => esc_html__("External URL", 'pabloguadi'),
					"description" => wp_kses_data( __("External URL for this TOC item", 'pabloguadi') ),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "separator",
					"heading" => esc_html__("Add separator", 'pabloguadi'),
					"description" => wp_kses_data( __("Add separator under item in the TOC", 'pabloguadi') ),
					"class" => "",
					"value" => array("Add separator" => "yes" ),
					"type" => "checkbox"
				),
				pabloguadi_get_vc_param('id')
			),
		) );
		
		class WPBakeryShortCode_Trx_Anchor extends PABLOGUADI_VC_ShortCodeSingle {}
	}
}
?>