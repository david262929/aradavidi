<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('pabloguadi_sc_price_block_theme_setup')) {
	add_action( 'pabloguadi_action_before_init_theme', 'pabloguadi_sc_price_block_theme_setup' );
	function pabloguadi_sc_price_block_theme_setup() {
		add_action('pabloguadi_action_shortcodes_list', 		'pabloguadi_sc_price_block_reg_shortcodes');
		if (function_exists('pabloguadi_exists_visual_composer') && pabloguadi_exists_visual_composer())
			add_action('pabloguadi_action_shortcodes_list_vc','pabloguadi_sc_price_block_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

if (!function_exists('pabloguadi_sc_price_block')) {	
	function pabloguadi_sc_price_block($atts, $content=null){	
		if (pabloguadi_in_shortcode_blogger()) return '';
		extract(pabloguadi_html_decode(shortcode_atts(array(
			// Individual params
			"style" => 1,
			"title" => "",
			"link" => "",
			"link_text" => "",
			"icon" => "",
			"money" => "",
			"currency" => "$",
			"period" => "",
			"align" => "",
			"scheme" => "",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"width" => "",
			"height" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$output = '';
		$class .= ($class ? ' ' : '') . pabloguadi_get_css_position_as_classes($top, $right, $bottom, $left);
		$css .= pabloguadi_get_css_dimensions_from_values($width, $height);
		if ($money) $money = do_shortcode('[trx_price money="'.esc_attr($money).'" period="'.esc_attr($period).'"'.($currency ? ' currency="'.esc_attr($currency).'"' : '').']');
		$content = do_shortcode(pabloguadi_sc_clear_around($content));
		$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
					. ' class="sc_price_block sc_price_block_style_'.max(1, min(3, $style))
						. (!empty($class) ? ' '.esc_attr($class) : '')
						. ($scheme && !pabloguadi_param_is_off($scheme) && !pabloguadi_param_is_inherit($scheme) ? ' scheme_'.esc_attr($scheme) : '') 
						. ($align && $align!='none' ? ' align'.esc_attr($align) : '') 
						. '"'
					. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
					. (!pabloguadi_param_is_off($animation) ? ' data-animation="'.esc_attr(pabloguadi_get_animation_classes($animation)).'"' : '')
					. '>'
				. (!empty($title) ? '<div class="sc_price_block_title"><span>'.($title).'</span></div>' : '')
				. '<div class="sc_price_block_money">'
					. (!empty($icon) ? '<div class="sc_price_block_icon '.esc_attr($icon).'"></div>' : '')
					. ($money)
				. '</div>'
				. (!empty($content) ? '<div class="sc_price_block_description">'.($content).'</div>' : '')
				. (!empty($link_text) ? '<div class="sc_price_block_link">'.do_shortcode('[trx_button link="'.($link ? esc_url($link) : '#').'"]'.($link_text).'[/trx_button]').'</div>' : '')
			. '</div>';
		return apply_filters('pabloguadi_shortcode_output', $output, 'trx_price_block', $atts, $content);
	}
	pabloguadi_require_shortcode('trx_price_block', 'pabloguadi_sc_price_block');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'pabloguadi_sc_price_block_reg_shortcodes' ) ) {
	//add_action('pabloguadi_action_shortcodes_list', 'pabloguadi_sc_price_block_reg_shortcodes');
	function pabloguadi_sc_price_block_reg_shortcodes() {
	
		pabloguadi_sc_map("trx_price_block", array(
			"title" => esc_html__("Price block", 'pabloguadi'),
			"desc" => wp_kses_data( __("Insert price block with title, price and description", 'pabloguadi') ),
			"decorate" => false,
			"container" => true,
			"params" => array(
				"style" => array(
					"title" => esc_html__("Block style", 'pabloguadi'),
					"desc" => wp_kses_data( __("Select style for this price block", 'pabloguadi') ),
					"value" => 1,
					"options" => pabloguadi_get_list_styles(1, 3),
					"type" => "checklist"
				),
				"title" => array(
					"title" => esc_html__("Title", 'pabloguadi'),
					"desc" => wp_kses_data( __("Block title", 'pabloguadi') ),
					"value" => "",
					"type" => "text"
				),
				"link" => array(
					"title" => esc_html__("Link URL", 'pabloguadi'),
					"desc" => wp_kses_data( __("URL for link from button (at bottom of the block)", 'pabloguadi') ),
					"value" => "",
					"type" => "text"
				),
				"link_text" => array(
					"title" => esc_html__("Link text", 'pabloguadi'),
					"desc" => wp_kses_data( __("Text (caption) for the link button (at bottom of the block). If empty - button not showed", 'pabloguadi') ),
					"value" => "",
					"type" => "text"
				),
				"icon" => array(
					"title" => esc_html__("Icon",  'pabloguadi'),
					"desc" => wp_kses_data( __('Select icon from Fontello icons set (placed before/instead price)',  'pabloguadi') ),
					"value" => "",
					"type" => "icons",
					"options" => pabloguadi_get_sc_param('icons')
				),
				"money" => array(
					"title" => esc_html__("Money", 'pabloguadi'),
					"desc" => wp_kses_data( __("Money value (dot or comma separated)", 'pabloguadi') ),
					"divider" => true,
					"value" => "",
					"type" => "text"
				),
				"currency" => array(
					"title" => esc_html__("Currency", 'pabloguadi'),
					"desc" => wp_kses_data( __("Currency character", 'pabloguadi') ),
					"value" => "$",
					"type" => "text"
				),
				"period" => array(
					"title" => esc_html__("Period", 'pabloguadi'),
					"desc" => wp_kses_data( __("Period text (if need). For example: monthly, daily, etc.", 'pabloguadi') ),
					"value" => "",
					"type" => "text"
				),
				"scheme" => array(
					"title" => esc_html__("Color scheme", 'pabloguadi'),
					"desc" => wp_kses_data( __("Select color scheme for this block", 'pabloguadi') ),
					"value" => "",
					"type" => "checklist",
					"options" => pabloguadi_get_sc_param('schemes')
				),
				"align" => array(
					"title" => esc_html__("Alignment", 'pabloguadi'),
					"desc" => wp_kses_data( __("Align price to left or right side", 'pabloguadi') ),
					"divider" => true,
					"value" => "",
					"type" => "checklist",
					"dir" => "horizontal",
					"options" => pabloguadi_get_sc_param('float')
				), 
				"_content_" => array(
					"title" => esc_html__("Description", 'pabloguadi'),
					"desc" => wp_kses_data( __("Description for this price block", 'pabloguadi') ),
					"divider" => true,
					"rows" => 4,
					"value" => "",
					"type" => "textarea"
				),
				"width" => pabloguadi_shortcodes_width(),
				"height" => pabloguadi_shortcodes_height(),
				"top" => pabloguadi_get_sc_param('top'),
				"bottom" => pabloguadi_get_sc_param('bottom'),
				"left" => pabloguadi_get_sc_param('left'),
				"right" => pabloguadi_get_sc_param('right'),
				"id" => pabloguadi_get_sc_param('id'),
				"class" => pabloguadi_get_sc_param('class'),
				"animation" => pabloguadi_get_sc_param('animation'),
				"css" => pabloguadi_get_sc_param('css')
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'pabloguadi_sc_price_block_reg_shortcodes_vc' ) ) {
	//add_action('pabloguadi_action_shortcodes_list_vc', 'pabloguadi_sc_price_block_reg_shortcodes_vc');
	function pabloguadi_sc_price_block_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_price_block",
			"name" => esc_html__("Price block", 'pabloguadi'),
			"description" => wp_kses_data( __("Insert price block with title, price and description", 'pabloguadi') ),
			"category" => esc_html__('Content', 'pabloguadi'),
			'icon' => 'icon_trx_price_block',
			"class" => "trx_sc_single trx_sc_price_block",
			"content_element" => true,
			"is_container" => false,
			"show_settings_on_create" => true,
			"params" => array(
				array(
					"param_name" => "style",
					"heading" => esc_html__("Block style", 'pabloguadi'),
					"desc" => wp_kses_data( __("Select style of this price block", 'pabloguadi') ),
					"admin_label" => true,
					"class" => "",
					"std" => 1,
					"value" => array_flip(pabloguadi_get_list_styles(1, 3)),
					"type" => "dropdown"
				),
				array(
					"param_name" => "title",
					"heading" => esc_html__("Title", 'pabloguadi'),
					"description" => wp_kses_data( __("Block title", 'pabloguadi') ),
					"admin_label" => true,
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "link",
					"heading" => esc_html__("Link URL", 'pabloguadi'),
					"description" => wp_kses_data( __("URL for link from button (at bottom of the block)", 'pabloguadi') ),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "link_text",
					"heading" => esc_html__("Link text", 'pabloguadi'),
					"description" => wp_kses_data( __("Text (caption) for the link button (at bottom of the block). If empty - button not showed", 'pabloguadi') ),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "icon",
					"heading" => esc_html__("Icon", 'pabloguadi'),
					"description" => wp_kses_data( __("Select icon from Fontello icons set (placed before/instead price)", 'pabloguadi') ),
					"class" => "",
					"value" => pabloguadi_get_sc_param('icons'),
					"type" => "dropdown"
				),
				array(
					"param_name" => "money",
					"heading" => esc_html__("Money", 'pabloguadi'),
					"description" => wp_kses_data( __("Money value (dot or comma separated)", 'pabloguadi') ),
					"admin_label" => true,
					"group" => esc_html__('Money', 'pabloguadi'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "currency",
					"heading" => esc_html__("Currency symbol", 'pabloguadi'),
					"description" => wp_kses_data( __("Currency character", 'pabloguadi') ),
					"admin_label" => true,
					"group" => esc_html__('Money', 'pabloguadi'),
					"class" => "",
					"value" => "$",
					"type" => "textfield"
				),
				array(
					"param_name" => "period",
					"heading" => esc_html__("Period", 'pabloguadi'),
					"description" => wp_kses_data( __("Period text (if need). For example: monthly, daily, etc.", 'pabloguadi') ),
					"admin_label" => true,
					"group" => esc_html__('Money', 'pabloguadi'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "scheme",
					"heading" => esc_html__("Color scheme", 'pabloguadi'),
					"description" => wp_kses_data( __("Select color scheme for this block", 'pabloguadi') ),
					"group" => esc_html__('Colors and Images', 'pabloguadi'),
					"class" => "",
					"value" => array_flip(pabloguadi_get_sc_param('schemes')),
					"type" => "dropdown"
				),
				array(
					"param_name" => "align",
					"heading" => esc_html__("Alignment", 'pabloguadi'),
					"description" => wp_kses_data( __("Align price to left or right side", 'pabloguadi') ),
					"admin_label" => true,
					"class" => "",
					"value" => array_flip(pabloguadi_get_sc_param('float')),
					"type" => "dropdown"
				),
				array(
					"param_name" => "content",
					"heading" => esc_html__("Description", 'pabloguadi'),
					"description" => wp_kses_data( __("Description for this price block", 'pabloguadi') ),
					"class" => "",
					"value" => "",
					"type" => "textarea_html"
				),
				pabloguadi_get_vc_param('id'),
				pabloguadi_get_vc_param('class'),
				pabloguadi_get_vc_param('animation'),
				pabloguadi_get_vc_param('css'),
				pabloguadi_vc_width(),
				pabloguadi_vc_height(),
				pabloguadi_get_vc_param('margin_top'),
				pabloguadi_get_vc_param('margin_bottom'),
				pabloguadi_get_vc_param('margin_left'),
				pabloguadi_get_vc_param('margin_right')
			),
			'js_view' => 'VcTrxTextView'
		) );
		
		class WPBakeryShortCode_Trx_PriceBlock extends PABLOGUADI_VC_ShortCodeSingle {}
	}
}
?>