<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('pabloguadi_sc_number_theme_setup')) {
	add_action( 'pabloguadi_action_before_init_theme', 'pabloguadi_sc_number_theme_setup' );
	function pabloguadi_sc_number_theme_setup() {
		add_action('pabloguadi_action_shortcodes_list', 		'pabloguadi_sc_number_reg_shortcodes');
		if (function_exists('pabloguadi_exists_visual_composer') && pabloguadi_exists_visual_composer())
			add_action('pabloguadi_action_shortcodes_list_vc','pabloguadi_sc_number_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_number id="unique_id" value="400"]
*/

if (!function_exists('pabloguadi_sc_number')) {	
	function pabloguadi_sc_number($atts, $content=null){	
		if (pabloguadi_in_shortcode_blogger()) return '';
		extract(pabloguadi_html_decode(shortcode_atts(array(
			// Individual params
			"value" => "",
			"align" => "",
			// Common params
			"id" => "",
			"class" => "",
			"animation" => "",
			"css" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$class .= ($class ? ' ' : '') . pabloguadi_get_css_position_as_classes($top, $right, $bottom, $left);
		$output = '<div' . ($id ? ' id="'.esc_attr($id).'"' : '') 
				. ' class="sc_number' 
					. (!empty($align) ? ' align'.esc_attr($align) : '') 
					. (!empty($class) ? ' '.esc_attr($class) : '') 
					. '"'
				. (!pabloguadi_param_is_off($animation) ? ' data-animation="'.esc_attr(pabloguadi_get_animation_classes($animation)).'"' : '')
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
				. '>';
		for ($i=0; $i < pabloguadi_strlen($value); $i++) {
			$output .= '<span class="sc_number_item">' . trim(pabloguadi_substr($value, $i, 1)) . '</span>';
		}
		$output .= '</div>';
		return apply_filters('pabloguadi_shortcode_output', $output, 'trx_number', $atts, $content);
	}
	pabloguadi_require_shortcode('trx_number', 'pabloguadi_sc_number');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'pabloguadi_sc_number_reg_shortcodes' ) ) {
	//add_action('pabloguadi_action_shortcodes_list', 'pabloguadi_sc_number_reg_shortcodes');
	function pabloguadi_sc_number_reg_shortcodes() {
	
		pabloguadi_sc_map("trx_number", array(
			"title" => esc_html__("Number", 'pabloguadi'),
			"desc" => wp_kses_data( __("Insert number or any word as set separate characters", 'pabloguadi') ),
			"decorate" => false,
			"container" => false,
			"params" => array(
				"value" => array(
					"title" => esc_html__("Value", 'pabloguadi'),
					"desc" => wp_kses_data( __("Number or any word", 'pabloguadi') ),
					"value" => "",
					"type" => "text"
				),
				"align" => array(
					"title" => esc_html__("Align", 'pabloguadi'),
					"desc" => wp_kses_data( __("Select block alignment", 'pabloguadi') ),
					"value" => "none",
					"type" => "checklist",
					"dir" => "horizontal",
					"options" => pabloguadi_get_sc_param('align')
				),
				"top" => pabloguadi_get_sc_param('top'),
				"bottom" => pabloguadi_get_sc_param('bottom'),
				"left" => pabloguadi_get_sc_param('left'),
				"right" => pabloguadi_get_sc_param('right'),
				"id" => pabloguadi_get_sc_param('id'),
				"class" => pabloguadi_get_sc_param('class'),
				"animation" => pabloguadi_get_sc_param('animation'),
				"css" => pabloguadi_get_sc_param('css')
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'pabloguadi_sc_number_reg_shortcodes_vc' ) ) {
	//add_action('pabloguadi_action_shortcodes_list_vc', 'pabloguadi_sc_number_reg_shortcodes_vc');
	function pabloguadi_sc_number_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_number",
			"name" => esc_html__("Number", 'pabloguadi'),
			"description" => wp_kses_data( __("Insert number or any word as set of separated characters", 'pabloguadi') ),
			"category" => esc_html__('Content', 'pabloguadi'),
			"class" => "trx_sc_single trx_sc_number",
			'icon' => 'icon_trx_number',
			"content_element" => true,
			"is_container" => false,
			"show_settings_on_create" => true,
			"params" => array(
				array(
					"param_name" => "value",
					"heading" => esc_html__("Value", 'pabloguadi'),
					"description" => wp_kses_data( __("Number or any word to separate", 'pabloguadi') ),
					"admin_label" => true,
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "align",
					"heading" => esc_html__("Alignment", 'pabloguadi'),
					"description" => wp_kses_data( __("Select block alignment", 'pabloguadi') ),
					"class" => "",
					"value" => array_flip(pabloguadi_get_sc_param('align')),
					"type" => "dropdown"
				),
				pabloguadi_get_vc_param('id'),
				pabloguadi_get_vc_param('class'),
				pabloguadi_get_vc_param('animation'),
				pabloguadi_get_vc_param('css'),
				pabloguadi_get_vc_param('margin_top'),
				pabloguadi_get_vc_param('margin_bottom'),
				pabloguadi_get_vc_param('margin_left'),
				pabloguadi_get_vc_param('margin_right')
			)
		) );
		
		class WPBakeryShortCode_Trx_Number extends PABLOGUADI_VC_ShortCodeSingle {}
	}
}
?>