<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('pabloguadi_sc_highlight_theme_setup')) {
	add_action( 'pabloguadi_action_before_init_theme', 'pabloguadi_sc_highlight_theme_setup' );
	function pabloguadi_sc_highlight_theme_setup() {
		add_action('pabloguadi_action_shortcodes_list', 		'pabloguadi_sc_highlight_reg_shortcodes');
		if (function_exists('pabloguadi_exists_visual_composer') && pabloguadi_exists_visual_composer())
			add_action('pabloguadi_action_shortcodes_list_vc','pabloguadi_sc_highlight_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_highlight id="unique_id" color="fore_color's_name_or_#rrggbb" backcolor="back_color's_name_or_#rrggbb" style="custom_style"]Et adipiscing integer, scelerisque pid, augue mus vel tincidunt porta[/trx_highlight]
*/

if (!function_exists('pabloguadi_sc_highlight')) {	
	function pabloguadi_sc_highlight($atts, $content=null){	
		if (pabloguadi_in_shortcode_blogger()) return '';
		extract(pabloguadi_html_decode(shortcode_atts(array(
			// Individual params
			"color" => "",
			"bg_color" => "",
			"font_size" => "",
			"type" => "1",
			// Common params
			"id" => "",
			"class" => "",
			"css" => ""
		), $atts)));
		$css .= ($color != '' ? 'color:' . esc_attr($color) . ';' : '')
			.($bg_color != '' ? 'background-color:' . esc_attr($bg_color) . ';' : '')
			.($font_size != '' ? 'font-size:' . esc_attr(pabloguadi_prepare_css_value($font_size)) . '; line-height: 1em;' : '');
		$output = '<span' . ($id ? ' id="'.esc_attr($id).'"' : '') 
				. ' class="sc_highlight'.($type>0 ? ' sc_highlight_style_'.esc_attr($type) : ''). (!empty($class) ? ' '.esc_attr($class) : '').'"'
				. ($css!='' ? ' style="'.esc_attr($css).'"' : '')
				. '>' 
				. do_shortcode($content) 
				. '</span>';
		return apply_filters('pabloguadi_shortcode_output', $output, 'trx_highlight', $atts, $content);
	}
	pabloguadi_require_shortcode('trx_highlight', 'pabloguadi_sc_highlight');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'pabloguadi_sc_highlight_reg_shortcodes' ) ) {
	//add_action('pabloguadi_action_shortcodes_list', 'pabloguadi_sc_highlight_reg_shortcodes');
	function pabloguadi_sc_highlight_reg_shortcodes() {
	
		pabloguadi_sc_map("trx_highlight", array(
			"title" => esc_html__("Highlight text", 'pabloguadi'),
			"desc" => wp_kses_data( __("Highlight text with selected color, background color and other styles", 'pabloguadi') ),
			"decorate" => false,
			"container" => true,
			"params" => array(
				"type" => array(
					"title" => esc_html__("Type", 'pabloguadi'),
					"desc" => wp_kses_data( __("Highlight type", 'pabloguadi') ),
					"value" => "1",
					"type" => "checklist",
					"options" => array(
						0 => esc_html__('Custom', 'pabloguadi'),
						1 => esc_html__('Type 1', 'pabloguadi'),
						2 => esc_html__('Type 2', 'pabloguadi'),
						3 => esc_html__('Type 3', 'pabloguadi')
					)
				),
				"color" => array(
					"title" => esc_html__("Color", 'pabloguadi'),
					"desc" => wp_kses_data( __("Color for the highlighted text", 'pabloguadi') ),
					"divider" => true,
					"value" => "",
					"type" => "color"
				),
				"bg_color" => array(
					"title" => esc_html__("Background color", 'pabloguadi'),
					"desc" => wp_kses_data( __("Background color for the highlighted text", 'pabloguadi') ),
					"value" => "",
					"type" => "color"
				),
				"font_size" => array(
					"title" => esc_html__("Font size", 'pabloguadi'),
					"desc" => wp_kses_data( __("Font size of the highlighted text (default - in pixels, allows any CSS units of measure)", 'pabloguadi') ),
					"value" => "",
					"type" => "text"
				),
				"_content_" => array(
					"title" => esc_html__("Highlighting content", 'pabloguadi'),
					"desc" => wp_kses_data( __("Content for highlight", 'pabloguadi') ),
					"divider" => true,
					"rows" => 4,
					"value" => "",
					"type" => "textarea"
				),
				"id" => pabloguadi_get_sc_param('id'),
				"class" => pabloguadi_get_sc_param('class'),
				"css" => pabloguadi_get_sc_param('css')
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'pabloguadi_sc_highlight_reg_shortcodes_vc' ) ) {
	//add_action('pabloguadi_action_shortcodes_list_vc', 'pabloguadi_sc_highlight_reg_shortcodes_vc');
	function pabloguadi_sc_highlight_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_highlight",
			"name" => esc_html__("Highlight text", 'pabloguadi'),
			"description" => wp_kses_data( __("Highlight text with selected color, background color and other styles", 'pabloguadi') ),
			"category" => esc_html__('Content', 'pabloguadi'),
			'icon' => 'icon_trx_highlight',
			"class" => "trx_sc_single trx_sc_highlight",
			"content_element" => true,
			"is_container" => false,
			"show_settings_on_create" => true,
			"params" => array(
				array(
					"param_name" => "type",
					"heading" => esc_html__("Type", 'pabloguadi'),
					"description" => wp_kses_data( __("Highlight type", 'pabloguadi') ),
					"admin_label" => true,
					"class" => "",
					"value" => array(
							esc_html__('Custom', 'pabloguadi') => 0,
							esc_html__('Type 1', 'pabloguadi') => 1,
							esc_html__('Type 2', 'pabloguadi') => 2,
							esc_html__('Type 3', 'pabloguadi') => 3
						),
					"type" => "dropdown"
				),
				array(
					"param_name" => "color",
					"heading" => esc_html__("Text color", 'pabloguadi'),
					"description" => wp_kses_data( __("Color for the highlighted text", 'pabloguadi') ),
					"class" => "",
					"value" => "",
					"type" => "colorpicker"
				),
				array(
					"param_name" => "bg_color",
					"heading" => esc_html__("Background color", 'pabloguadi'),
					"description" => wp_kses_data( __("Background color for the highlighted text", 'pabloguadi') ),
					"class" => "",
					"value" => "",
					"type" => "colorpicker"
				),
				array(
					"param_name" => "font_size",
					"heading" => esc_html__("Font size", 'pabloguadi'),
					"description" => wp_kses_data( __("Font size for the highlighted text (default - in pixels, allows any CSS units of measure)", 'pabloguadi') ),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "content",
					"heading" => esc_html__("Highlight text", 'pabloguadi'),
					"description" => wp_kses_data( __("Content for highlight", 'pabloguadi') ),
					"class" => "",
					"value" => "",
					"type" => "textarea_html"
				),
				pabloguadi_get_vc_param('id'),
				pabloguadi_get_vc_param('class'),
				pabloguadi_get_vc_param('css')
			),
			'js_view' => 'VcTrxTextView'
		) );
		
		class WPBakeryShortCode_Trx_Highlight extends PABLOGUADI_VC_ShortCodeSingle {}
	}
}
?>