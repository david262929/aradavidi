<?php

/* Theme setup section
-------------------------------------------------------------------- */
if (!function_exists('pabloguadi_sc_button_theme_setup')) {
	add_action( 'pabloguadi_action_before_init_theme', 'pabloguadi_sc_button_theme_setup' );
	function pabloguadi_sc_button_theme_setup() {
		add_action('pabloguadi_action_shortcodes_list', 		'pabloguadi_sc_button_reg_shortcodes');
		if (function_exists('pabloguadi_exists_visual_composer') && pabloguadi_exists_visual_composer())
			add_action('pabloguadi_action_shortcodes_list_vc','pabloguadi_sc_button_reg_shortcodes_vc');
	}
}



/* Shortcode implementation
-------------------------------------------------------------------- */

/*
[trx_button id="unique_id" type="square|round" fullsize="0|1" style="global|light|dark" size="mini|medium|big|huge|banner" icon="icon-name" link='#' target='']Button caption[/trx_button]
*/

if (!function_exists('pabloguadi_sc_button')) {	
	function pabloguadi_sc_button($atts, $content=null){	
		if (pabloguadi_in_shortcode_blogger()) return '';
		extract(pabloguadi_html_decode(shortcode_atts(array(
			// Individual params
			"type" => "square",
			"style" => "filled",
			"size" => "small",
			"icon" => "",
			"color" => "",
			"bg_color" => "",
			"link" => "",
			"target" => "",
			"align" => "",
			"rel" => "",
			"popup" => "no",
			// Common params
			"id" => "",
			"class" => "",
			"css" => "",
			"animation" => "",
			"width" => "",
			"height" => "",
			"top" => "",
			"bottom" => "",
			"left" => "",
			"right" => ""
		), $atts)));
		$class .= ($class ? ' ' : '') . pabloguadi_get_css_position_as_classes($top, $right, $bottom, $left);
		$css .= pabloguadi_get_css_dimensions_from_values($width, $height)
			. ($color !== '' ? 'color:' . esc_attr($color) .';' : '')
			. ($bg_color !== '' ? 'background-color:' . esc_attr($bg_color) . '; border-color:'. esc_attr($bg_color) .';' : '');
		if (pabloguadi_param_is_on($popup)) pabloguadi_enqueue_popup('magnific');
		$output = '<a href="' . (empty($link) ? '#' : $link) . '"'
			. (!empty($target) ? ' target="'.esc_attr($target).'"' : '')
			. (!empty($rel) ? ' rel="'.esc_attr($rel).'"' : '')
			. (!pabloguadi_param_is_off($animation) ? ' data-animation="'.esc_attr(pabloguadi_get_animation_classes($animation)).'"' : '')
			. ' class="sc_button sc_button_' . esc_attr($type) 
					. ' sc_button_style_' . esc_attr($style) 
					. ' sc_button_size_' . esc_attr($size)
					. ($align && $align!='none' ? ' align'.esc_attr($align) : '') 
					. (!empty($class) ? ' '.esc_attr($class) : '')
					. ($icon!='' ? '  sc_button_iconed '. esc_attr($icon) : '') 
					. (pabloguadi_param_is_on($popup) ? ' sc_popup_link' : '') 
					. '"'
			. ($id ? ' id="'.esc_attr($id).'"' : '') 
			. ($css!='' ? ' style="'.esc_attr($css).'"' : '') 
			. '>'
			. do_shortcode($content)
			. '</a>';
		return apply_filters('pabloguadi_shortcode_output', $output, 'trx_button', $atts, $content);
	}
	pabloguadi_require_shortcode('trx_button', 'pabloguadi_sc_button');
}



/* Register shortcode in the internal SC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'pabloguadi_sc_button_reg_shortcodes' ) ) {
	//add_action('pabloguadi_action_shortcodes_list', 'pabloguadi_sc_button_reg_shortcodes');
	function pabloguadi_sc_button_reg_shortcodes() {
	
		pabloguadi_sc_map("trx_button", array(
			"title" => esc_html__("Button", 'pabloguadi'),
			"desc" => wp_kses_data( __("Button with link", 'pabloguadi') ),
			"decorate" => false,
			"container" => true,
			"params" => array(
				"_content_" => array(
					"title" => esc_html__("Caption", 'pabloguadi'),
					"desc" => wp_kses_data( __("Button caption", 'pabloguadi') ),
					"value" => "",
					"type" => "text"
				),
				"type" => array(
					"title" => esc_html__("Button's shape", 'pabloguadi'),
					"desc" => wp_kses_data( __("Select button's shape", 'pabloguadi') ),
					"value" => "square",
					"size" => "medium",
					"options" => array(
						'square' => esc_html__('Square', 'pabloguadi'),
						'round' => esc_html__('Round', 'pabloguadi')
					),
					"type" => "switch"
				), 
				"style" => array(
					"title" => esc_html__("Button's style", 'pabloguadi'),
					"desc" => wp_kses_data( __("Select button's style", 'pabloguadi') ),
					"value" => "default",
					"dir" => "horizontal",
					"options" => array(
						'filled' => esc_html__('Filled', 'pabloguadi'),
						'border' => esc_html__('Border', 'pabloguadi')
					),
					"type" => "checklist"
				), 
				"size" => array(
					"title" => esc_html__("Button's size", 'pabloguadi'),
					"desc" => wp_kses_data( __("Select button's size", 'pabloguadi') ),
					"value" => "small",
					"dir" => "horizontal",
					"options" => array(
						'small' => esc_html__('Small', 'pabloguadi'),
						'medium' => esc_html__('Medium', 'pabloguadi'),
						'large' => esc_html__('Large', 'pabloguadi')
					),
					"type" => "checklist"
				), 
				"icon" => array(
					"title" => esc_html__("Button's icon",  'pabloguadi'),
					"desc" => wp_kses_data( __('Select icon for the title from Fontello icons set',  'pabloguadi') ),
					"value" => "",
					"type" => "icons",
					"options" => pabloguadi_get_sc_param('icons')
				),
				"color" => array(
					"title" => esc_html__("Button's text color", 'pabloguadi'),
					"desc" => wp_kses_data( __("Any color for button's caption", 'pabloguadi') ),
					"std" => "",
					"value" => "",
					"type" => "color"
				),
				"bg_color" => array(
					"title" => esc_html__("Button's backcolor", 'pabloguadi'),
					"desc" => wp_kses_data( __("Any color for button's background", 'pabloguadi') ),
					"value" => "",
					"type" => "color"
				),
				"align" => array(
					"title" => esc_html__("Button's alignment", 'pabloguadi'),
					"desc" => wp_kses_data( __("Align button to left, center or right", 'pabloguadi') ),
					"value" => "none",
					"type" => "checklist",
					"dir" => "horizontal",
					"options" => pabloguadi_get_sc_param('align')
				), 
				"link" => array(
					"title" => esc_html__("Link URL", 'pabloguadi'),
					"desc" => wp_kses_data( __("URL for link on button click", 'pabloguadi') ),
					"divider" => true,
					"value" => "",
					"type" => "text"
				),
				"target" => array(
					"title" => esc_html__("Link target", 'pabloguadi'),
					"desc" => wp_kses_data( __("Target for link on button click", 'pabloguadi') ),
					"dependency" => array(
						'link' => array('not_empty')
					),
					"value" => "",
					"type" => "text"
				),
				"popup" => array(
					"title" => esc_html__("Open link in popup", 'pabloguadi'),
					"desc" => wp_kses_data( __("Open link target in popup window", 'pabloguadi') ),
					"dependency" => array(
						'link' => array('not_empty')
					),
					"value" => "no",
					"type" => "switch",
					"options" => pabloguadi_get_sc_param('yes_no')
				), 
				"rel" => array(
					"title" => esc_html__("Rel attribute", 'pabloguadi'),
					"desc" => wp_kses_data( __("Rel attribute for button's link (if need)", 'pabloguadi') ),
					"dependency" => array(
						'link' => array('not_empty')
					),
					"value" => "",
					"type" => "text"
				),
				"width" => pabloguadi_shortcodes_width(),
				"height" => pabloguadi_shortcodes_height(),
				"top" => pabloguadi_get_sc_param('top'),
				"bottom" => pabloguadi_get_sc_param('bottom'),
				"left" => pabloguadi_get_sc_param('left'),
				"right" => pabloguadi_get_sc_param('right'),
				"id" => pabloguadi_get_sc_param('id'),
				"class" => pabloguadi_get_sc_param('class'),
				"animation" => pabloguadi_get_sc_param('animation'),
				"css" => pabloguadi_get_sc_param('css')
			)
		));
	}
}


/* Register shortcode in the VC Builder
-------------------------------------------------------------------- */
if ( !function_exists( 'pabloguadi_sc_button_reg_shortcodes_vc' ) ) {
	//add_action('pabloguadi_action_shortcodes_list_vc', 'pabloguadi_sc_button_reg_shortcodes_vc');
	function pabloguadi_sc_button_reg_shortcodes_vc() {
	
		vc_map( array(
			"base" => "trx_button",
			"name" => esc_html__("Button", 'pabloguadi'),
			"description" => wp_kses_data( __("Button with link", 'pabloguadi') ),
			"category" => esc_html__('Content', 'pabloguadi'),
			'icon' => 'icon_trx_button',
			"class" => "trx_sc_single trx_sc_button",
			"content_element" => true,
			"is_container" => false,
			"show_settings_on_create" => true,
			"params" => array(
				array(
					"param_name" => "content",
					"heading" => esc_html__("Caption", 'pabloguadi'),
					"description" => wp_kses_data( __("Button caption", 'pabloguadi') ),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "type",
					"heading" => esc_html__("Button's shape", 'pabloguadi'),
					"description" => wp_kses_data( __("Select button's shape", 'pabloguadi') ),
					"class" => "",
					"value" => array(
						esc_html__('Square', 'pabloguadi') => 'square',
						esc_html__('Round', 'pabloguadi') => 'round'
					),
					"type" => "dropdown"
				),
				array(
					"param_name" => "style",
					"heading" => esc_html__("Button's style", 'pabloguadi'),
					"description" => wp_kses_data( __("Select button's style", 'pabloguadi') ),
					"class" => "",
					"value" => array(
						esc_html__('Filled', 'pabloguadi') => 'filled',
						esc_html__('Border', 'pabloguadi') => 'border'
					),
					"type" => "dropdown"
				),
				array(
					"param_name" => "size",
					"heading" => esc_html__("Button's size", 'pabloguadi'),
					"description" => wp_kses_data( __("Select button's size", 'pabloguadi') ),
					"admin_label" => true,
					"class" => "",
					"value" => array(
						esc_html__('Small', 'pabloguadi') => 'small',
						esc_html__('Medium', 'pabloguadi') => 'medium',
						esc_html__('Large', 'pabloguadi') => 'large'
					),
					"type" => "dropdown"
				),
				array(
					"param_name" => "icon",
					"heading" => esc_html__("Button's icon", 'pabloguadi'),
					"description" => wp_kses_data( __("Select icon for the title from Fontello icons set", 'pabloguadi') ),
					"class" => "",
					"value" => pabloguadi_get_sc_param('icons'),
					"type" => "dropdown"
				),
				array(
					"param_name" => "color",
					"heading" => esc_html__("Button's text color", 'pabloguadi'),
					"description" => wp_kses_data( __("Any color for button's caption", 'pabloguadi') ),
					"class" => "",
					"value" => "",
					"type" => "colorpicker"
				),
				array(
					"param_name" => "bg_color",
					"heading" => esc_html__("Button's backcolor", 'pabloguadi'),
					"description" => wp_kses_data( __("Any color for button's background", 'pabloguadi') ),
					"class" => "",
					"value" => "",
					"type" => "colorpicker"
				),
				array(
					"param_name" => "align",
					"heading" => esc_html__("Button's alignment", 'pabloguadi'),
					"description" => wp_kses_data( __("Align button to left, center or right", 'pabloguadi') ),
					"class" => "",
					"value" => array_flip(pabloguadi_get_sc_param('align')),
					"type" => "dropdown"
				),
				array(
					"param_name" => "link",
					"heading" => esc_html__("Link URL", 'pabloguadi'),
					"description" => wp_kses_data( __("URL for the link on button click", 'pabloguadi') ),
					"class" => "",
					"group" => esc_html__('Link', 'pabloguadi'),
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "target",
					"heading" => esc_html__("Link target", 'pabloguadi'),
					"description" => wp_kses_data( __("Target for the link on button click", 'pabloguadi') ),
					"class" => "",
					"group" => esc_html__('Link', 'pabloguadi'),
					"value" => "",
					"type" => "textfield"
				),
				array(
					"param_name" => "popup",
					"heading" => esc_html__("Open link in popup", 'pabloguadi'),
					"description" => wp_kses_data( __("Open link target in popup window", 'pabloguadi') ),
					"class" => "",
					"group" => esc_html__('Link', 'pabloguadi'),
					"value" => array(esc_html__('Open in popup', 'pabloguadi') => 'yes'),
					"type" => "checkbox"
				),
				array(
					"param_name" => "rel",
					"heading" => esc_html__("Rel attribute", 'pabloguadi'),
					"description" => wp_kses_data( __("Rel attribute for the button's link (if need", 'pabloguadi') ),
					"class" => "",
					"group" => esc_html__('Link', 'pabloguadi'),
					"value" => "",
					"type" => "textfield"
				),
				pabloguadi_get_vc_param('id'),
				pabloguadi_get_vc_param('class'),
				pabloguadi_get_vc_param('animation'),
				pabloguadi_get_vc_param('css'),
				pabloguadi_vc_width(),
				pabloguadi_vc_height(),
				pabloguadi_get_vc_param('margin_top'),
				pabloguadi_get_vc_param('margin_bottom'),
				pabloguadi_get_vc_param('margin_left'),
				pabloguadi_get_vc_param('margin_right')
			),
			'js_view' => 'VcTrxTextView'
		) );
		
		class WPBakeryShortCode_Trx_Button extends PABLOGUADI_VC_ShortCodeSingle {}
	}
}
?>